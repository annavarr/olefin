# Resistive Python Chambers (olefin)

olefin is a python package written to analyse data from Resistive Plate Chambers
(RPC) detectors for the EP-DT-FS group. The package and the analysis style
reflects the data acquisition and layout of the setup.

## Installation

At the time of writing this package is on the branch `v2`. You can install it
via pip using:

```python
!pip install --user --upgrade git+https://gitlab.cern.ch/grigolet/olefin.git@v2#egg=olefin
```

Once install it you can check if the installation was fine by running:

```python
import olefin
print(olefin.__version__)
```

    2.0.0

# Documentation

Documentation for the analysis package can be found in the [notebooks folder](notebooks/)
