from libc.stdio cimport getline, FILE, fopen, fclose
from libc.stdlib cimport atoi
import numpy as np
cimport numpy as cnp
cimport cython

cnp.import_array()

cdef int count_lines(char* filepath):
    cdef char* fname = filepath
    cdef FILE* cfile
    cfile = fopen(fname, "r")
    if cfile == NULL:
        raise FileNotFoundError(2, "No such file or directory: '%s'" % fname)
    
    cdef char* line = NULL
    cdef size_t l = 0
    cdef ssize_t read
    cdef int lines = 0
    
    while True:
        read = getline(&line, &l, cfile)
        if read == -1: break
        lines += 1
    fclose(cfile)
    
    return lines

@cython.boundscheck(False)  # Deactivate bounds checking
@cython.wraparound(False)   # Deactivate negative indexing.
def read_file(char* filepath, int header):
    cdef char* fname = filepath
    cdef int lines = count_lines(filepath)
    cdef cnp.int16_t[::1] values
    cdef int effective_lines = <int>(lines - lines / (header+7) * 7)
    
    values = np.empty(effective_lines, dtype=np.int16)
    
    cdef FILE* cfile
    cfile = fopen(fname, "r")
    if cfile == NULL:
        raise FileNotFoundError(2, "No such file or directory: '%s'" % fname)
    
    cdef char* line = NULL
    cdef size_t l = 0
    cdef ssize_t read
    cdef int line_num = 0
    
    while True:
        read = getline(&line, &l, cfile)
        if read == -1: break
        if line[0] in [b'R', b'B', b'C', b'E', b'P', b'T', b'D']:
            continue
        values[line_num] = atoi(line)
        line_num += 1

    fclose(cfile)
    return values