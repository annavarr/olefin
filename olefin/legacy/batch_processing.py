import telegram
import json
import os
from functions import configs, parse_excel_runs, analyse_and_save
from processing_configs import *
import pandas as pd
import logging

# Parse the single configuration variables

telegram_enabled = False
if telegram_bot_token and telegram_chat_id:
    telegram_enabled = True
    bot = telegram.Bot(token="731706800:AAHxjWZzypn7zbSPnQ3w-lHsL7fNQyryM4Q")
else:
    logging.info("Telegram not enabled because token/chat are not set")

configs_test_beam = {
    **configs,
    "time_min": 150,
    "time_max": 200,
    "fit_params": [0.6, 0.01, 10200],
}
configs_cosmics = {**configs, "time_min": 340, "time_max": 370, "resistance": 56}
configs_4096 = {
    **configs,
    "time_min": 3150,
    "time_max": 3250,
    "header": 4100,
    "no_header": False,
}

cosmics = parse_excel_runs(excel_path_cosmics, append="/eos/user/r/rpcgif/BackupRPC/")
test_beam = parse_excel_runs(
    excel_path_test_beam,
    replace_from="C:/data",
    replace_to="/eos/user/r/rpcgif/BackupRPC/gif",
)

if data_to_analyse == "cosmics":
    df = cosmics
elif data_to_analyse == "test_beam":
    df = test_beam
elif data_to_analyse == "both":
    df = pd.concat([cosmics, test_beam])

df_eff = pd.DataFrame()
if rows_to_analyse:
    df = df.iloc[rows_to_analyse]
if from_row or to_row:
    df = df.iloc[from_row:to_row]
for ix, row in df.iterrows():
    try:
        if not pd.isna(row["notes"]) and "RPC2" in row["notes"]:
            configs = {
                **configs,
                "charge_factor": 0.122 * 2 / 56,
            }  # 56 Ohm instead of 50
        if (
            row["name"] == "20181220_no-header-512_Cosmics3"
        ):  # Acquisition set at 0.5Vpp instead of 2Vpp
            configs = {
                **configs,
                "adc_to_mv": 0.03,
                "charge_factor": 0.03 * 2 / 56,
                "noise_thresh": 48,
                "charge_thresh": 36,
            }
        if "header-4096" in row["name"]:
            configs = configs_4096
        logging.info(f'Analysing {row["name"]}')
        _, df_eff_info = analyse_and_save(
            save_path, configs, row, force=True, lite=True, force_eff=True
        )
        df_eff = df_eff.append(df_eff_info)
        bot.sendMessage(telegram_chat_id, f'✅Analysed:\n{row["name"]}')
    except Exception as e:
        message = f'❌Failed:\n{row["name"]}'
        logging.error(e)
        if telegram_enabled:
            bot.sendMessage(telegram_chat_id, message)

df_eff.to_csv(save_path / f"df_eff_{data_to_analyse}.csv")
if telegram_enabled:
    bot.sendMessage(telegram_chat_id, f"Done with {data_to_analyse} analysis")
