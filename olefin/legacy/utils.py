import re
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd


def parse_macro(filepath):
    with open(filepath) as file:
        plots = {}
        current_graph = "eff"
        numgraph = 0
        foundprob = False
        for line in file:
            if "new TGraphErrors" in line:
                numgraph += 1
                if numgraph not in plots.keys():
                    plots[numgraph] = {
                        "hv": [],
                        "eff": [],
                        "prob": [],
                        "legend": None,
                        "graphs": [],
                    }

            if "SetName" in line:
                regroup = re.search(r"SetName\(\"(.+)\"\)", line)
                graph = regroup.group(1)
                plots[numgraph]["graphs"].append(graph)

            if "gre->SetLineStyle" in line:
                if line[21] == "2" and not foundprob:
                    current_graph = "prob"
                    numgraph = 1
                    foundprob = True

            if "gre->SetPoint(" in line:
                regroup = re.search(r"SetPoint\(\d+,(\d+),(\d*.\d*)\)", line)
                hv = regroup.group(1)
                eff = regroup.group(2)
                if int(hv) not in plots[numgraph]["hv"]:
                    plots[numgraph]["hv"].append(int(hv))
                plots[numgraph][current_graph].append(float(eff))

            # add also legend if  exist
            if "leg->AddEntry(" in line:
                regroup = re.search(r"AddEntry\(\"(.+)\",\"(.+)\",\"(.+)\"\)", line)
                graphname = regroup.group(1)
                legend = regroup.group(2)
                # search where the graph is
                for key, plot in plots.items():
                    if graphname in plot["graphs"]:
                        plots[key]["legend"] = legend

    return plots


def macro_to_df(filepath, append=""):
    data = parse_macro(filepath)[1]
    df = pd.DataFrame(
        {
            f"{append}hv": data["hv"],
            f"{append}eff": data["eff"],
            f"{append}prob": data["prob"],
        }
    )
    return df
