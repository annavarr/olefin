import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.lines import Line2D
import numpy as np
from cycler import cycler
from scipy.signal import savgol_filter, find_peaks
import matplotlib.font_manager as font_manager

from cycler import cycler
from .functions import (
    fit_efficiency,
    get_hv_efficiency_knee,
    slope,
    sigmoid_function,
    calc_fwhm_time,
)


def set_square():
    mpl.rc("figure", figsize=(4, 4))


def set_rect():
    mpl.rc("figure", figsize=(6, 4))


def set_grid(nrows, ncols, shape="square"):
    width = 4 if shape == "square" else 6
    mpl.rc("figure", figsize=(width * ncols, 4 * nrows))


def set_xlabel(text, ax=None):
    if ax:
        ax.set_xlabel(text, ha="right", x=1.0)
    else:
        plt.xlabel(text, ha="right", x=1.0)


def set_ylabel(text, ax=None):
    if ax:
        ax.set_ylabel(text, ha="right", y=1.0)
    else:
        plt.ylabel(text, ha="right", y=1.0)


def root_palette():
    return [
        "#000000",
        "#ff0000",
        "#00ff00",
        "#0000ff",
        "#ffff00",
        "#ff00ff",
        "#00ffff",
        "#59d354",
        "#5954d8",
        "#fefefe",
        "#c0b6ac",
        "#4c4c4c",
        "#666666",
        "#7f7f7f",
        "#999999",
        "#b2b2b2",
        "#cccccc",
        "#e5e5e5",
        "#f2f2f2",
        "#ccc6aa",
        "#ccc6aa",
        "#c1bfa8",
        "#bab5a3",
        "#b2a596",
        "#b7a39b",
        "#ad998c",
        "#9b8e82",
        "#876656",
        "#afcec6",
        "#84c1a3",
        "#829e8c",
        "#adbcc6",
        "#7a8e99",
        "#758991",
    ]


def set_palette(which="root"):
    if which == "root":
        mpl.rcParams["axes.prop_cycle"] = cycler(color=root_palette())
    elif which == "mpl":
        mpl.rcParams["axes.prop_cycle"] = cycler(color=plt.cm.tab10.colors)
    else:
        mpl.rcParams["axes.prop_cycle"] = cycler(color=which)


def set_root_style(figsize=(4, 4), verbose=False):
    import os

    script_dir = os.path.dirname(__file__)
    font_dirs = [
        os.path.join(script_dir, "freefont-20051206/"),
    ]
    font_files = font_manager.findSystemFonts(fontpaths=font_dirs)
    font_list = font_manager.createFontList(font_files)
    font_manager.fontManager.ttflist.extend(font_list)
    if verbose:
        print(font_files)
        print(font_list)
    mpl.rcParams["axes.prop_cycle"] = cycler(
        color=root_palette()
    )  # set ROOT color palette
    mpl.rcParams["font.family"] = "FreeSans"  # GNU freefont used by root
    mpl.rc("figure", figsize=figsize, dpi=100)  # default layout for square plot id 4, 4
    plt.rcParams["font.weight"] = "bold"  # make ticks bold
    mpl.rcParams["axes.labelweight"] = "bold"  # make labels bold
    mpl.rc("xtick.minor", visible=True, size=4, top=True)  # long minor and major ticks
    mpl.rc("xtick.major", size=8)
    mpl.rc("ytick.minor", visible=True, size=4, right=True)
    mpl.rc("ytick.major", size=8)
    mpl.rc("lines", linewidth=0.8)  # thin lines
    mpl.rc("errorbar", capsize=1.5)  # small errorbars
    mpl.rcParams["grid.linestyle"] = "--"
    mpl.rcParams["axes.formatter.useoffset"] = False
    mpl.rc(
        "legend",
        loc="best",
        numpoints=1,
        handlelength=2,
        shadow=False,
        fontsize="small",
        handletextpad=1.2,
        borderaxespad=1,
        fancybox=False,
    )


mpl.rcParams["grid.color"] = "k"
mpl.rcParams["grid.linestyle"] = "--"
mpl.rcParams["grid.linewidth"] = 0.5
mpl.rcParams["errorbar.capsize"] = 3
mpl.rcParams["boxplot.flierprops.marker"] = "+"
mpl.rcParams["boxplot.whiskerprops.linestyle"] = "--"
mpl.rcParams["boxplot.meanprops.marker"] = "^"
mpl.rcParams["boxplot.meanprops.markersize"] = 6
mpl.rcParams["boxplot.meanprops.linestyle"] = "--"
mpl.rcParams["boxplot.meanprops.linewidth"] = 1.0
mpl.rcParams["legend.fancybox"] = False
mpl.rcParams["legend.framealpha"] = None
mpl.rcParams["legend.scatterpoints"] = 3
mpl.rcParams["legend.edgecolor"] = "inherit"
mpl.rcParams["xtick.direction"] = "in"
mpl.rcParams["ytick.direction"] = "in"
mpl.rcParams["xtick.top"] = True
mpl.rcParams["ytick.right"] = True
mpl.rcParams["axes.formatter.offset_threshold"] = 2
mpl.rc("figure", figsize=(12, 6), dpi=150)
mpl.rc("axes", grid=True)
mpl.rc("hist", bins="auto")


def plot_height_distribution(df, save=False, filepath="height_distribution.png"):
    fig = plt.figure()
    plt.subplot(121)
    plt.title("Height distribution")
    df.query(
        'efficient_noise_event == True & fired == True & type == "avalanche"'
    ).groupby("voltage")["height"].hist(
        histtype="step", bins=np.arange(0, 80, 1), edgecolor="C0", label="avalanche"
    )
    df.query(
        'efficient_noise_event == True & fired == True & type == "streamer"'
    ).groupby("voltage")["height"].hist(
        histtype="step", bins=np.arange(0, 80, 1), edgecolor="C1", label="streamer"
    )
    custom_lines = [
        Line2D([0], [0], color="C0", lw=4),
        Line2D([0], [0], color="C1", lw=4),
    ]
    plt.legend(custom_lines, ["Avalanche", "Streamer"])
    plt.xlabel("Height [mV]")
    plt.grid(linestyle=":")

    plt.subplot(122)
    plt.title("Mean height")
    plt.ylabel("Height [mV]")
    df.query('fired == True & type == "avalanche" & might_be_noise == False').groupby(
        "voltage"
    )["height"].mean().plot(marker="o", linestyle="--", label="avalanche")
    df.query('fired == True & type == "streamer" & might_be_noise == False').groupby(
        "voltage"
    )["height"].mean().plot(marker="o", linestyle="--", label="streamer")
    plt.grid(linestyle=":")
    if save:
        plt.savefig(filepath, bbox_inches="tight")

    return fig


def plot_charge_distribution(df, save=False, filepath="charge_distribution.png"):
    fig = plt.figure()
    plt.subplot(121)
    plt.title("Charge distribution")
    df.query(
        'efficient_noise_event == True & fired == True & type == "avalanche"'
    ).groupby("voltage")["charge"].hist(
        histtype="step", bins=np.arange(0, 30, 0.5), edgecolor="C0", label="avalanche"
    )
    df.query(
        'efficient_noise_event == True & fired == True & type == "streamer"'
    ).groupby("voltage")["charge"].hist(
        histtype="step", bins=np.arange(0, 30, 0.5), edgecolor="C1", label="streamer"
    )
    custom_lines = [
        Line2D([0], [0], color="C0", lw=4),
        Line2D([0], [0], color="C1", lw=4),
    ]
    plt.legend(custom_lines, ["Avalanche", "Streamer"])
    plt.xlabel("Charge [pC]")
    plt.grid(linestyle=":")

    plt.subplot(122)
    plt.title("Mean charge")
    plt.ylabel("Charge [pC]")
    df.query('fired == True & type == "avalanche" & might_be_noise == False').groupby(
        "voltage"
    )["charge"].mean().plot(marker="o", linestyle="--", label="avalanche")
    df.query('fired == True & type == "streamer" & might_be_noise == False').groupby(
        "voltage"
    )["charge"].mean().plot(marker="o", linestyle="--", label="streamer")
    plt.grid(linestyle=":")
    if save:
        plt.savefig(filepath, bbox_inches="tight")
    return fig


def plot_effprob(df, save=False, filepath="effprob.png"):
    fig = plt.figure()
    plt.subplot(121)
    plt.suptitle("Efficiency and streamer probability")

    hv = df.voltage.unique()
    eff = df.groupby("voltage").efficiency.mean().values
    eff_error = df.groupby("voltage").efficiency_error.mean().values
    streamer_probability = df.groupby("voltage").streamer_probability.mean()
    stprob_error = np.sqrt(
        streamer_probability * (1 - streamer_probability) / df.nevent.max()
    )

    plt.errorbar(hv, eff, yerr=eff_error, fmt="-")
    plt.errorbar(hv, streamer_probability, stprob_error, hv_err, "--")
    plt.grid(linestyle=":")
    plt.ylim(0, 1)
    plt.xlim(df.voltage.min() - 200, df.voltage.max() + 200)
    if save:
        plt.savefig(filepath, bbox_inches="tight")

    return fig


def plot_effprob_fit(
    df,
    save=False,
    filepath="effprob_fit.png",
    fit_result=True,
    top_position=0.54,
    initial_params=[0.95, 0.01, 9500],
    pad=2,
    hverr=20,
    ax=None,
):
    hv = df.voltage.unique()
    eff = df.groupby("voltage").efficiency.mean().values
    eff_error = df.groupby("voltage").efficiency_error.mean().values
    streamer_probability = df.groupby("voltage").streamer_probability.mean()
    stprob_error = np.sqrt(
        streamer_probability * (1 - streamer_probability) / df.nevent.max()
    )

    fig = plt.figure()
    effmax, gamma, hv50, chi2, prob = fit_efficiency(hv, eff, eff_error, initial_params)
    knee = get_hv_efficiency_knee(effmax, gamma, hv50)
    stprob_knee = np.interp(knee, hv, streamer_probability)
    eff_strob_knee = (
        stprob_knee / effmax
    )  # since effmax is not always 1 it makes more sense to use this value
    x = np.arange(df.voltage.min() - 200, df.voltage.max() + 200, 10)
    y = sigmoid_function(x, effmax, gamma, hv50)
    m, q = slope(effmax, gamma, hv50)
    yslope = m * x + q

    if ax:
        ax.plot(x, y, "-")
        ax.errorbar(hv, eff, xerr=hverr, yerr=eff_error, fmt="C1.")
        ax.errorbar(hv, streamer_probability, stprob_error, hverr, "C0.--")
    else:
        plt.plot(x, y, "-")
        plt.errorbar(hv, eff, xerr=hverr, yerr=eff_error, fmt="C1.")
        plt.errorbar(hv, streamer_probability, stprob_error, hverr, "C0.--")
    plt.grid(linestyle=":")
    plt.axvline(hv50)
    plt.axvline(knee)
    if fit_result:
        textstr = "\n".join(
            [
                f"effmax = {effmax:.2f}",
                f"hv50% = {hv50:.0f}V",
                f"hvknee = {knee:.0f}V",
                f"stprob $knee_{{eff}}$ = {eff_strob_knee:.2f}",
            ]
        )
        plt.text(
            knee + 100,
            top_position,
            textstr,
            fontsize=11,
            verticalalignment="top",
            bbox=dict(facecolor="white", alpha=1, pad=pad),
        )
    plt.xlim(df.voltage.min() - 200, df.voltage.max() + 200)
    plt.ylim(0, 1)
    plt.xlabel("High Voltage [V]")
    plt.ylabel("Efficiency")
    if save:
        plt.savefig(filepath, bbox_inches="tight")

    return fig, plt.gca()


def plot_cluster_size_distribution(
    df, save=False, filepath="cluster_size_distribution.png"
):
    plt.rc(
        "axes",
        prop_cycle=(
            cycler("marker", [".", "v", "^", "X", "*", "P", "D", "H", "p", "s"])
            + cycler("color", plt.cm.tab10(np.linspace(0, 1, 10)))
        ),
    )
    fig = plt.figure()
    plt.title("Cluster size distribution")
    plt.subplot(121)
    plt.cm.tab10(np.linspace(0, 1, 10))
    df_filtered = df.query(
        "fired == True & might_be_noise == False & isin_time_window == True"
    )
    for ix, group in df_filtered.groupby("voltage"):
        data = group["cluster_size"]
        y, bin_edges = np.histogram(data, bins=range(1, 9))
        bincenters = 0.5 * (bin_edges[1:] + bin_edges[:-1]) - 0.5
        plt.plot(bincenters, y, label=f"{ix}V", linestyle="--")

    plt.xticks(range(1, 8))
    set_ylabel("Counts")
    plt.legend()
    plt.xlabel("# fired 2mm strips")
    plt.grid(linestyle=":")
    set_xlabel("Cluster size")
    plt.legend(loc="best")

    plt.subplot(122)
    hv = df.voltage.unique()
    hv_err = 10
    clu_dist = df.query("is_detected").groupby("voltage").cluster_size
    clu_size = clu_dist.mean()
    clu_err = clu_dist.std() / 10
    plt.errorbar(hv, clu_size, clu_err, hv_err, "C0.--")
    plt.grid(linestyle=":")
    set_ylabel("Cluster size")
    set_xlabel("Voltage [V]")
    if save:
        plt.savefig(filepath, bbox_inches="tight")

    return fig


def plot_signal_examples(df, configs, save=False, filepath="signal_examples.png"):
    fig = plt.figure(figsize=(14, 20), dpi=130)
    sample_test = df[df.voltage == df.voltage.max()]
    plt.suptitle(f"Example of signals at {df.voltage.max()}V")
    for ixplot in range(9):
        plt.subplot(3, 3, ixplot + 1)
        nevent = (
            sample_test.query("efficient_noise_event == True")
            .sample()["nevent"]
            .iloc[0]
        )
        event_data = sample_test[sample_test.nevent == nevent]
        for strip, group in event_data.groupby("strip"):
            row = group.iloc[0]
            data = np.array(row["data"])
            plt.plot(data - row["baseline"] + strip * 200, label=strip)
            plt.axhline(
                -configs["height_thresh"] / configs["adc_to_mv"] + strip * 200,
                color="C7",
            )
    if save:
        plt.savefig(filepath, bbox_inches="tight")

    return fig


def plot_time_resolution_distribution(
    df, save=False, filepath="time_resolution.png", bins=(50, 100)
):
    fig = plt.figure()
    plt.subplot(121)
    df_filtered = df.query(
        "fired == True & might_be_noise == False & isin_time_window == True"
    )
    for ix, group in df_filtered.groupby("voltage"):
        data = group["time_resolution"] * 2
        y, bin_edges = np.histogram(data, bins=range(bins[0], bins[1], 2))
        bincenters = 0.5 * (bin_edges[1:] + bin_edges[:-1]) - 0.5
        plt.plot(bincenters, y, label=ix, linestyle="--")
    plt.legend(loc="best")
    plt.xlabel("Time [ns]")
    plt.ylabel("# events")

    plt.subplot(122)
    plt.title("FWHM of time resolution distribution")
    df_filtered.groupby("voltage").apply(calc_fwhm_time).plot(
        marker="o", linestyle="--"
    )
    plt.grid(linestyle=":")
    plt.ylabel("FWHM Time [ns]")
    plt.xlabel("Voltage [V]")
    if save:
        plt.savefig(filepath, bbox_inches="tight")

    return fig


def plot_scatter_h_c(df, save=False, filepath="h_c_scatter.png"):
    fig = plt.figure()
    plt.title("Charge vs Height")
    av = df.query(
        'might_be_noise == False & isin_time_window == True & fired == True & type == "avalanche"'
    )
    av.plot.scatter(x="charge", y="height", s=0.1, color="C0", alpha=0.5, ax=plt.gca())
    st = df.query(
        'might_be_noise == False & isin_time_window == True & fired == True & type == "streamer"'
    )
    st.plot.scatter(x="charge", y="height", s=0.1, color="C1", alpha=0.5, ax=plt.gca())
    custom_lines = [
        Line2D([0], [0], color="C0", lw=4),
        Line2D([0], [0], color="C1", lw=4),
    ]
    plt.legend(custom_lines, ["Avalanche", "Streamer"])
    plt.xlabel("Charge [pC]")
    plt.ylabel("Height [mV]")
    plt.xlim(0, 100)
    plt.ylim(0, 100)
    plt.grid(linestyle=":")
    if save:
        plt.savefig(filepath, bbox_inches="tight")

    return fig


def plot_h_c_ratio_distribution(df, save=False, filepath="h_c_ratio_distribution.png"):
    dfcopy = df.copy()
    dfcopy["ratio"] = dfcopy.height / dfcopy.charge
    dfcopy["ratio"].replace([np.inf, -np.inf], np.nan)
    fig = plt.figure()
    plt.title("Charge [pC] / Height [mV] ratio distribution")
    dfcopy.query("fired == True & might_be_noise == False & isin_time_window == True")[
        "ratio"
    ].hist(bins=300, histtype="step")
    if save:
        plt.savefig(filepath, bbox_inches="tight")

    return fig


def plot_strip_data_peaks(strip_data, window=31, pol=3, height=5, distance=80):
    fig, ax = plt.subplots()
    strip_data = -strip_data
    strip_data = strip_data - strip_data.mean()
    ax.plot(strip_data, alpha=0.5, label="digitizer data")
    filtered = savgol_filter(strip_data, window, pol, mode="interp")
    peaks, properties = find_peaks(filtered, height=height, distance=distance)
    ax.plot(filtered, label="smooth data")
    ax.plot(peaks, filtered[peaks], "o", label="found peaks")
    ax.legend()

    return fig, ax
