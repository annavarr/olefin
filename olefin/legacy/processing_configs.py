from pathlib import Path

excel_path_test_beam = Path("/eos/user/r/rpcgif/BackupRPC/gif/all/test_beam.xlsx")
excel_path_cosmics = Path("/eos/user/r/rpcgif/BackupRPC/gif/cosmics.xlsx")
save_path = Path("/eos/user/r/rpcgif/BackupRPC/output-data/")
data_to_analyse = "cosmics"
telegram_bot_token = None
telegram_chat_id = None
rows_to_analyse = None
from_row = None
to_row = None
