import pandas as pd
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
from tqdm import tqdm
from scipy.signal import savgol_filter, find_peaks
import logging
import ROOT
import root_numpy as rn
from numba import jit
from multiprocessing import Pool, cpu_count

try:
    import pyximport

    # this compile at import time the files with .pyx extension
    pyximport.install(
        setup_args={"include_dirs": np.get_include()},
        language_level=3,
        reload_support=True,
    )
    from olefin.legacy.read_header_file import read_file
except:
    logging.error(f"Could not import read_header_file extension")

# for data lab with header:
# 'time_min': 3150, 'time_max': 3250, 'header': 4100, 'no_header': False

configs = {
    "adc_to_mv": 0.122,  # the conversion factor of the digitizer
    "charge_thresh_av": 5,  # above this pC charge values signals will be considered streamers
    "height_thresh_av": 12,  # above this mV height values signals will be considereed streamers
    "height_thresh": 2,  # threshold in mV above which to consider a signal an actaual event
    "time_sample": 2,  # the sampling time of the digitizer
    "resistance": 50,  # the resistance of the terminated strips
    "charge_factor": 0.122
    * 2
    / 50,  # the conversion to get the charge from the area of the signal
    "charge_thresh": 9,  # the threshold in adc above which to sum the values to compute the charge
    "noise_thresh": 9,  # the threshold in adc to check if a signal has some reflections
    "noise_counts": 2,  # the number of maximum allowed reflections for a signal to be considered an event,
    "time_min": 280,  # from where to start looking for an event, in samples
    "time_max": 400,  # until where to look for an event, in samples
    "header": 520,  # the record length from wavedump
    "no_header": True,  # if the wavefile doens't have an header for each event recorded
    "cut": None,  # if the data to be read is too long (like header 4100) a slice will be applied  to save memory
    "fit_params": [0.9, 0.01, 10500],  # the initial parameters for fitting efficiency
    "smart_time_window": False,  # find the mode of the time distribution and apply a window [mode - 30, mode + 50] window,
    "channels_map": {
        "TR_0_0": {"type": "RPC", "number": -1, "strip": -1},
        "wave0": {"type": "RPC", "number": 2, "strip": 1},
        "wave1": {"type": "RPC", "number": 2, "strip": 2},
        "wave2": {"type": "RPC", "number": 2, "strip": 3},
        "wave3": {"type": "RPC", "number": 2, "strip": 4},
        "wave4": {"type": "RPC", "number": 2, "strip": 5},
        "wave5": {"type": "RPC", "number": 2, "strip": 6},
        "wave6": {"type": "RPC", "number": 2, "strip": 7},
        "wave9": {"type": "RPC", "number": 4, "strip": 1},
        "wave10": {"type": "RPC", "number": 4, "strip": 2},
        "wave11": {"type": "RPC", "number": 4, "strip": 3},
        "wave12": {"type": "RPC", "number": 4, "strip": 4},
        "wave13": {"type": "RPC", "number": 4, "strip": 5},
        "wave14": {"type": "RPC", "number": 4, "strip": 6},
        "wave15": {"type": "RPC", "number": 4, "strip": 7},
        "wave17": {"type": "RPC", "number": 5, "strip": 1},
        "wave18": {"type": "RPC", "number": 5, "strip": 2},
        "wave19": {"type": "RPC", "number": 5, "strip": 3},
        "wave20": {"type": "RPC", "number": 5, "strip": 4},
        "wave21": {"type": "RPC", "number": 5, "strip": 5},
        "wave22": {"type": "RPC", "number": 5, "strip": 6},
        "wave23": {"type": "RPC", "number": 5, "strip": 7},
    },
}

configs_test_beam = {
    **configs,
    "time_min": 150,
    "time_max": 200,
    "fit_params": [0.6, 0.01, 10200],
    "baseline_interval": [0, 100],
}

configs_cosmics = {
    **configs,
    "time_min": 150,
    "time_max": 250,
    "fit_params": [0.6, 0.01, 10200],
}

configs_256 = {
    **configs,
    "adc_to_mv": 0.122,  # the conversion factor of the digitizer
    "charge_thresh_av": 5,  # above this pC charge values signals will be considered streamers
    "height_thresh_av": 13,  # above this mV height values signals will be considereed streamers
    "height_thresh": 2,  # threshold in mV above which to consider a signal an actaual event
    "time_sample": 2,  # the sampling time of the digitizer
    "resistance": 56,  # the resistance of the terminated strips
    "charge_factor": 0.122
    * 2
    / 56,  # the conversion to get the charge from the area of the signal
    "charge_thresh": 9,  # the threshold in adc above which to sum the values to compute the charge
    "noise_thresh": 12,  # the threshold in adc to check if a signal has some reflections
    "noise_counts": 2,  # the number of maximum allowed reflections for a signal to be considered an event,
    "time_min": 340,  # from where to start looking for an event, in samples
    "time_max": 370,  # until where to look for an event, in samples,
    "cfd_threshold": 0.4,
}


def analyse_run_folder(run_folder_path, configs=None):
    """Return two dataframes, the first containing data for each hv, rpc, strip, event,
    while the second one will have only global data about a the hv point (like the efficiency, streamer prob, etc.)

    Arguments:
        run_folder_path {string} -- Path of the run, like C:/data/20180918/no-header-512/SourceOffBeamOn
        other_data {pd.Series} -- the single row of the excel file containing the info about the run
    """
    df = pd.DataFrame()
    run_folder_path = Path(run_folder_path)
    parameters_path = tuple(run_folder_path.glob("**/parameters.dat"))
    if not parameters_path:
        # Try with the first letter uppercase
        parameters_path = tuple(run_folder_path.glob("**/Parameters.dat"))[0]
    else:
        parameters_path = tuple(run_folder_path.glob("**/parameters.dat"))[0]
    logging.debug(f"Found parameters at {parameters_path}")
    # First, get the parameters file and get the HV0 -> voltage association
    dfpar = pd.read_csv(parameters_path, header=None, delimiter="\t")
    dfpar["hv"] = dfpar.iloc[:, 1].apply(lambda x: f"HV{x}")
    dfpar.rename(columns={2: "voltage"}, inplace=True)
    hv_values = dfpar[["hv", "voltage"]]
    hv_list = hv_values.to_dict(orient="records")
    other_hv_args = {"configs": configs, "run_folder_path": run_folder_path}
    hv_args = [{**hv_item, **other_hv_args} for hv_item in hv_list]
    # workers = configs.get('workers', min(cpu_count(), len(hv_list)))
    for hv_arg in tqdm(hv_args, ncols=100, ascii=True, desc="Progress"):
        result = analyse_full_hv(hv_arg)
        df = df.append(result)

    df[["hv", "voltage", "type"]] = df[["hv", "voltage", "type"]].astype("category")
    logging.debug(
        f"Memory usage of the whole dataframe in Mbytes: {df.memory_usage(deep=True).sum() / 1e6}"
    )

    return df


def analyse_full_hv(hv_args):
    configs = hv_args["configs"]
    run_folder_path = hv_args["run_folder_path"]
    # construct the hv path
    hv_folder = tuple(run_folder_path.rglob(hv_args["hv"]))[0]
    if hv_folder.exists():
        logging.debug(f"Starting analysis of folder {hv_folder.stem}")
        results = analyse_single_hv_folder(hv_folder, configs)
        results["hv"] = hv_args["hv"]
        results["voltage"] = hv_args["voltage"]

        # Save some space by making data categorical
        results["hv"] = results["hv"].astype("category")
        results["voltage"] = results["voltage"].astype("category")
        logging.debug(f"Finished the analysis of folder {hv_folder.stem}")
        logging.debug(
            f"Memory usage of the dataframe: {results.memory_usage(deep=True).sum() / 1e6:1f} Mb"
        )

    return results


def read_currents(row):
    """Return a dataframe with the following columns:
    1. HV - int
    2. RPC0 - float
    3. RPC1 - float
    4. Beam - boolean"""
    currents_path = Path(row.loc["currents"])
    computed_sheetname = Path(row.loc["path"]).parts[-1]
    # Some excel files contains more than one run. Other ones have only a
    # sheet with the default name 'sheet1'
    try:
        df = pd.read_excel(currents_path, sheet_name=computed_sheetname)
    except:
        print("Reading default filename because ", computed_sheetname, " was not found")
        df = pd.read_excel(currents_path)

    # Some columns have trailing whitespace
    df.rename(columns=lambda x: x.strip(), inplace=True)
    # Check that pandas is not parsing also the timestamp or other values
    # that are not currents or hv
    # Take the first index (if any) where there are all nan values. Slice
    # the dataframe until that row
    nan_rows = df.index[df.isnull().all(1)]

    # From the excel file retrieve only the columns of the hv and the currents
    dfclean = pd.DataFrame()
    for column in ["HV", "RPC0", "RPC1", "RPC2", "RPC0-beam", "RPC1-beam"]:
        if column in df:
            dfclean[column] = df[column]

    # Slice the database to exclude all nan and other possible rows
    if len(nan_rows):
        dfclean = dfclean.iloc[: nan_rows[0], :]
    return dfclean


def analyse_single_hv_folder(hv_folder_path, configs={}):
    """Return a dataframe continaing the analysed data for all the strips, events

    Arguments:
        hv_folder_path {string} -- path of the HV0 or HV1, or HV{num}. folder
    """
    adc_to_mv = configs.get("adc_to_mv", 0.122)  # mV/adc
    charge_thresh_av = configs.get("charge_thresh_av", 5)  # pC
    height_thresh_av = configs.get("height_thresh_av", 12)  # mV
    height_thresh = configs.get("height_thresh", 3)  # mV
    time_sample = configs.get("time_sample", 2)  # ns/sample
    resistance = configs.get("resistance", 50)  # in Ohm
    charge_factor = adc_to_mv * time_sample / resistance
    charge_thresh = configs.get("charge_thresh", 9)  # in adc
    noise_thresh = configs.get("noise_thresh", 10)  # in adc
    noise_counts = configs.get("noise_counts", 3)  # pure number
    time_min = configs.get("time_min", 150)  # in adc
    time_max = configs.get("time_max", 200)  # in adc
    header = configs.get("header", 520)
    no_header = configs.get("no_header", True)
    cut = configs.get("cut", None)
    smart_time_window = configs.get("smart_time_window", False)
    heights_ratio_threshold = configs.get("heights_ratio_threshold", 1)
    heights_difference_threshold = configs.get("heights_difference_threshold", -1)
    baseline_interval = configs.get("baseline_interval", [0, 250])
    lite = configs.get("lite", False)
    trigger_height = configs.get("trigger_height", 4000)
    trigger_closest_to = configs.get("trigger_closest_to", 334)
    channels_map = configs.get("channels_map", None)
    # cfd_threshold = configs.get('cfd_threshold', 0.3)

    df = read_raw_data(
        hv_folder_path,
        header=header,
        no_header=no_header,
        cut=cut,
        channels_map=channels_map,
    )
    finaldf = pd.DataFrame()
    triggerdata = df[df.rpc == -1].copy()
    triggerdata["position"] = triggerdata["data"].apply(
        calc_trigger_position, args=(trigger_height, trigger_closest_to)
    )
    rpcdata = df[df.rpc != -1].copy()
    for rpc, rpcgroup in rpcdata.groupby("rpc"):
        rpcdf = rpcgroup.copy()
        rpcdf["time_trigger"] = (
            rpcdata.groupby("nevent")
            .apply(
                lambda x: pd.DataFrame(
                    {"index": x.index, "fired": triggerdata["position"][x["nevent"]]}
                )
            )
            .set_index("index")
        )
        rpcdf["minimum"] = calc_minimum(rpcdf["data"])
        rpcdf["maximum"] = calc_maximum(rpcdf["data"])
        rpcdf["time_minimum"] = calc_time_minimum(rpcdf["data"])
        # if smart_time_window is enabled find the signals between [mode - 30, mode + 50]
        if smart_time_window:
            mode = rpcdf["time_minimum"].mode()
            time_min = mode - 30
            time_max = mode + 50
        logging.debug("Computed time_trigger, minimum and time_minimum")

        # First: compute all the variables that don't need any discrimination
        rpcdf["baseline"] = calc_baseline(rpcdf["data"], baseline_interval)
        rpcdf["height"] = (rpcdf["baseline"] - rpcdf["minimum"]) * adc_to_mv
        rpcdf["inverse_height"] = (rpcdf["maximum"] - rpcdf["baseline"]) * adc_to_mv
        rpcdf["charge"] = calc_charge(rpcdf[["data", "baseline"]], charge_thresh)
        rpcdf["charge"] = rpcdf["charge"] * charge_factor
        rpcdf["heights_ratio"] = rpcdf["height"] / rpcdf["inverse_height"]
        rpcdf["time_resolution"] = rpcdf["time_trigger"] - rpcdf["time_minimum"]
        rpcdf["reflections_count"] = rpcdf[["data", "baseline"]].apply(
            lambda x: reflections_count(x[0], x[1], time_min, time_max, noise_thresh),
            axis=1,
        )
        logging.debug("Computed first set of fundamental variables")

        if lite:
            rpcdf.drop(columns=["data"], inplace=True)

        # Second: compute all the derived filter variables
        rpcdf["type"] = calc_signal_type(
            rpcdf[["charge", "height"]], charge_thresh_av, height_thresh_av
        )
        logging.debug("Computed type of signals")
        rpcdf["fired"] = is_fired(rpcdf["height"], height_thresh)
        logging.debug("Computed if fired or not")
        rpcdf["isin_time_window"] = rpcdf["time_minimum"].apply(
            lambda x: isin_time_window(x, time_min, time_max)
        )
        logging.debug("Computed isin_time_window")
        rpcdf["might_be_noise"] = rpcdf["reflections_count"] >= noise_counts
        logging.debug("Computed might_be_noise")
        rpcdf["is_signal"] = (
            rpcdf["fired"] & ~rpcdf["might_be_noise"] & rpcdf["isin_time_window"]
        )
        logging.debug("Computed is_signal")
        rpcdf["is_fat_signal"] = (
            rpcdf["is_signal"]
            & (rpcdf["heights_ratio"] < heights_ratio_threshold)
            & (rpcdf["inverse_height"] - rpcdf["height"] > heights_difference_threshold)
        )
        logging.debug("Computed is_fat_signal")
        rpcdf["is_detected"] = rpcdf["is_signal"] & ~rpcdf["is_fat_signal"]
        logging.debug("Computed is_detected")
        rpcdf["cluster_size"] = calc_cluster_size(
            rpcdf[["nevent", "is_detected"]], "is_detected"
        )
        logging.debug("Computed cluster_size")
        rpcdf["type"] = np.where(rpcdf["is_detected"], rpcdf["type"], "none")

        # Third: compute all the single value variables from a single hv point
        fwhm_time = calc_fwhm_time(rpcdf.query("is_detected"))
        eff, eff_error = calc_efficiency(
            rpcdf[["nevent", "is_detected"]], "is_detected"
        )
        streamer_probability = calc_streamer_probability(
            rpcdf[["nevent", "type", "is_detected"]], "is_detected"
        )
        streamer_probability_sum = calc_streamer_probability2(
            rpcdf[["nevent", "is_detected", "charge"]], "is_detected", charge_thresh_av
        )
        logging.debug("Computed third set of variables")

        # Fourth: add the single value variables to the df
        rpcdf["efficiency"] = eff
        rpcdf["streamer_probability"] = streamer_probability
        rpcdf["streamer_probability_sum"] = streamer_probability_sum
        rpcdf["efficiency_error"] = eff_error
        rpcdf["fwhm_time"] = fwhm_time
        logging.debug("Computed eff, streamer probability, eff_error and fwhm time")

        finaldf = finaldf.append(rpcdf)

    return finaldf


# ------------- Atomic functions for analysis algorithm ---------------


def calc_minimum(data):
    return data.apply(lambda x: min(x))


def calc_maximum(data):
    return data.apply(lambda x: max(x))


def calc_time_minimum(data):
    return data.apply(lambda x: np.argmin(np.array(x)))


def calc_signal_type(data, charge_thresh_av=5, height_thresh_av=12):
    return data.apply(
        lambda x: discriminate(x[0], x[1], charge_thresh_av, height_thresh_av), axis=1
    ).astype("category")


def might_be_noise(counts, thresh=10, counts_thresh=3):
    if counts > counts_thresh:
        return True
    return False


def reflections_count(data, baseline, time_min, time_max, thresh):
    data = np.array(data)[time_min:time_max]
    counts = (data - baseline > thresh).sum()
    return counts


def discriminate(charge, height, charge_thresh=5, height_thresh=10):
    if np.isnan(height):
        return "none"
    if (charge > charge_thresh) & (height > height_thresh):
        return "streamer"
    return "avalanche"


def calc_trigger_position(triggerdata, height=4000, closest_to=334):
    event = np.array(triggerdata)
    min_position = event.min()
    max_position = event.max()
    falling_mask = (event[:-1] > height) & (event[1:] < height)
    positions = np.flatnonzero(falling_mask) + 1
    if len(positions):
        # Return the position closest to the defined variable
        return positions[(np.abs(positions - closest_to)).argmin()]

    return 0  # it means the algorithm has failed or there has not been a real trigger


@jit
def find_intersection(data, baseline, minimum, threshold):
    level = baseline - threshold * (baseline - minimum)
    for index in range(len(data)):
        if data[index] > level and data[index + 1] < level:
            break

    x1, x2 = index, index + 1
    y1, y2 = data[x1], data[x2]
    x_intersection = (level - (y1 - (y2 - y1) * x1)) / (y2 - y1)
    return pd.Series([x1, x2, x_intersection, level])


def calc_cfd_time(rpcdata, threshold=0.3):
    return rpcdata.apply(
        lambda x: find_intersection(x[0], x[1], x[2], threshold), axis=1
    )


def is_efficient_event(rpcdata, exclude_noise=False, isin_time_window=True):
    if exclude_noise:
        return (
            rpcdata.groupby("nevent")
            .apply(
                lambda x: pd.DataFrame(
                    {
                        "index": x.index,
                        "fired": np.any(
                            x["fired"] & ~x["might_be_noise"] & x["isin_time_window"]
                        ),
                    }
                )
            )
            .set_index("index")
        )
    return (
        rpcdata.groupby("nevent")
        .apply(lambda x: pd.DataFrame({"index": x.index, "fired": np.any(x["fired"])}))
        .set_index("index")
    )


def isin_time_window(times, ixmin=150, ixmax=200):
    return ixmin < times < ixmax


def is_fired(heights, thresh=3):
    return heights > thresh


def calc_efficiency(data, column):
    """Return the efficiency and the error for a 3d array of the
    shape (strip, event, sample)

    Arguments:
        data {np.ndarray} -- 3d array of the shape (strip, event, sample)

    Keyword Arguments:
        thresh {int} -- Number of adc above the threshold for which to find the signal (default: {12})
        ixslice {int} -- Index from where to start looking for a (default: {100})

    Returns:
        (tuple) -- efficiency and error, from a poisson distribution
    """
    efficiency_counts = data.groupby("nevent")[column].any().sum()
    total_counts = data.nevent.unique().size
    efficiency = efficiency_counts / total_counts
    error = np.sqrt(efficiency * (1 - efficiency) / total_counts)
    return efficiency, error


def read_header_file(file_handle):
    values = []
    for line in file_handle:
        if line[0] in ["R", "B", "C", "E", "P", "T", "D"]:
            continue
        values.append(int(line))
    return np.array(values)


def read_raw_data(folder_path, header=520, no_header=True, cut=None, channels_map=None):
    """Read the data from a run folder and return a dataframe with
    (nevent, strip, data)

    Arguments:
        folder_path {string} -- path where with the folder, like C:/data/20181026/no-header-512/SourceOffBeamOn

    Keyword Arguments:
        header {int} -- record_length from wavedump digitizer (default: {520})

    Returns:
        pd.DataFrame -- of the shape (nevent, strip, data). Data is a list
    """

    folder_path = Path(folder_path)
    rpcdata = pd.DataFrame()
    for wavefile in folder_path.glob("*.txt"):
        if ".sys.v#." in str(wavefile):
            continue  # it's a file of eos
        wave_stem = wavefile.stem
        if no_header:
            dfwave = pd.read_csv(wavefile, header=None, names=["wave"])
            wavedata = dfwave["wave"].values
        else:
            dfwave = np.array(read_file(bytes(wavefile), header))
            wavedata = dfwave
        if cut:
            wavedata = np.copy(wavedata[:, cut[0] : cut[1]])
        n_events = wavedata.size / header
        events = pd.Series(np.split(wavedata, n_events))
        # added feature:
        if channels_map:
            # First, extract the trigger
            wave_object = channels_map.get(wave_stem, None)
            # If there is a wavefile that is not in the configs
            # jump to the next iteartion
            if not wave_object:
                continue
            rpc = wave_object["number"]
            strip = wave_object["strip"]
        else:
            nwave = int(wavefile.stem.strip("wave"))
            if nwave == 0:
                rpc = -1  # for trigger
                strip = -1  # for trigger
            elif 1 <= nwave <= 7:
                rpc = 0
                strip = nwave
            elif 8 <= nwave <= 14:
                rpc = 1
                strip = nwave - 7

        df = pd.DataFrame({"nevent": events.index.values})
        df["rpc"] = rpc
        df["strip"] = strip
        df["data"] = events
        rpcdata = rpcdata.append(df).sort_values(by=["rpc", "strip", "nevent"])
    rpcdata.reset_index(drop=True, inplace=True)
    return rpcdata


def select_event(rpcdata, num_event):
    """Select a single event and return ad dataframe

    Arguments:
        rpcdata {pd.DataFrame} -- (nevent, rpc, strip, data)
        num_event {int} -- shouldn't be bigger than 899

    Returns:
        pd.DataFrame -- of the shape (strip, nsample)
    """
    data = np.array(rpcdata[(rpcdata.rpc == 0) & (rpcdata.nevent == 440)].data.tolist())
    return data


def select_strip(rpcdata, num_strip):
    """Select all the data from a single strip

    Arguments:
        rpcdata {pd.DataFrame} -- (nevent, rpc, strip, data)
        num_strip {int} -- should be from 0 to 6

    Returns:
        pd.DataFrame -- of the shape (nevents, nsample)
    """

    data = np.array(rpcdata[(rpcdata.rpc == 0) & (rpcdata.strip == 5)].data.tolist())
    return data


def calc_baseline(data, baseline_interval=[0, 250]):
    """Return a dataframe with a column of the baseline for each row

    Arguments:
        rpcdata {pd.DataFrame} -- (nevent, rpc, strip, data)

    Keyword Arguments:
        ixslice {int} -- max index for which to calculate the baseline (default: {100})

    Returns:
        pd.Series
    """
    return data.apply(
        lambda x: np.array(x[baseline_interval[0] : baseline_interval[1]]).mean()
    )


def delete_useless_signals(rpcdata, has_signal):
    """For every row that doesn't have a signal delete the data and put np.nan. It should be
    more resources friendly in terms of memory

    Arguments:
        rpcdata {[type]} -- [description]
        has_signal {bool} -- [description]
    """

    rpcdata["baseline"] = rpcdata["data"].apply(
        lambda x: np.array(x[:maxixbaseline]).mean()
    )
    rpcdata = rpcdata[rpcdata.rpc == 0]
    # Add the baseline for each row as a new column
    # Sort values for correct indexing
    rpcdata = rpcdata.sort_values(by=["nevent", "strip"])
    # Use the has_signal variable of the shape (strips, nevents) as a mask
    # Select the index values for which a signal has been detected
    fired_index = rpcdata.index[has_signal.ravel().astype(bool)]
    # Now transform the dataframe in the following way:
    # 1. If the row is inthe fired_index keep it as it is
    # 2. If it's not put [] or np.nan in the data column
    rpcdata.loc[~has_signal.ravel().astype(bool), "data"] = np.nan
    return rpcdata


def calc_height(rpcdata, adc_to_mv=0.122):
    """Compute the height of the data for each event and strip

    Arguments:
        rpcdata {pd.DataFrame} -- should have (nevent, rpc, strip, data, baseline) in this order
        adc_to_mv {float} -- The conversion factor from adc to mV

    Returns:
        pd.DataFrame -- a 'minimum' column will be appended
    """

    rpcdata["minimum"] = rpcdata[["data", "baseline"]].apply(
        lambda x: -(x[1] - np.array(x[0])).min(), axis=1
    )
    rpcdata["height"] = rpcdata.minimum * adc_to_mv
    return rpcdata


def charge(event_data, baseline, thresh=12, factor=0.122 * 2 / 50):
    event_data = np.array(event_data, dtype=np.int16)
    filtered = np.where(event_data < baseline - thresh, baseline - event_data, 0)
    integral = np.sum(filtered)
    return integral


def calc_charge(data, charge_thresh):
    """Compute the charge for the entire dataframe

    Arguments:
        rpcdata {pd.DataFrame} -- shape (nevent, rpc, strip, data, baseline, minimum, height)

    Returns:
        rpcdata {pd.DataFrame} -- shape (nevent, rpc, strip, data, baseline, minimum, height, charge)
    """

    return data.apply(lambda x: charge(x["data"], x["baseline"], charge_thresh), axis=1)


def calc_time_resolution(rpcdata):
    """Return the dataframe with a column 'time_min', indicating position of the peak, in ns

    Arguments:
        rpcdata {pd.DataFrame} -- dataframe in the shape (nevent,rpc,strip,data,baseline,minimum)
    """

    rpcdata["time_min"] = rpcdata[["data", "baseline"]].apply(
        lambda x: (x[1] - np.array(x[0])).argmin(), axis=1
    )
    rpcdata["time_min"] = rpcdata["time_min"] * 2  # the result will be in ns.


def reduce_df_data(rpcdata):
    """Delete the 'data' column of the dataframe to save some memory

    Arguments:
        rpcdata {[type]} -- [description]
    """

    rpcdata.drop(columns="data", inplace=True)
    return rpcdata


def calc_streamer_probability(rpcdata, column):
    event_counts = rpcdata.groupby("nevent")[column].any().sum()
    streamer_counts = (
        rpcdata.query(column)
        .groupby("nevent")
        .apply(lambda x: np.any(x.type == "streamer"))
        .sum()
    )
    if event_counts == 0:
        return 0
    return streamer_counts / event_counts


def calc_streamer_probability2(rpcdata, column, charge_thresh_av=5):
    tot_charges = rpcdata.query(f"{column}").groupby("nevent").charge.sum()
    fraction = tot_charges[tot_charges > charge_thresh_av]
    event_counts = rpcdata.groupby("nevent")[column].any().sum()
    if event_counts == 0:
        return 0
    return fraction.size / event_counts


def cluster_size(event):
    diff = np.ediff1d(event.astype(np.int8))
    clusters = np.split(event, np.where(diff == -1)[0] + 1)
    return max(map(np.sum, clusters))


def calc_cluster_size(rpcdata, column):
    return (
        rpcdata.groupby("nevent")
        .apply(
            lambda x: pd.DataFrame({"index": x.index, "fired": cluster_size(x[column])})
        )
        .set_index("index")
    )


def set_fired(rpcdata):
    rpcdata["fired"] = (~pd.isna(rpcdata["height"])).astype(int)
    return rpcdata


def calc_fwhm_time(rpcdata, column="time_resolution"):
    times = rpcdata[column]
    if np.isnan(times).all():
        return np.nan
    min_value = times.min()
    max_value = times.max()
    bins = int(max_value - min_value)
    hist = ROOT.TH1I("time_distribution", "Trigger times", bins, min_value, max_value)
    rn.fill_hist(hist, times.values)
    hist.Fit("gaus", "Q")
    fit_func = hist.GetListOfFunctions().FindObject("gaus")
    std = fit_func.GetParameter(2) * 2  # remember the ns/sample
    fwhm = std * np.sqrt(2 * np.log(2))

    return fwhm


def filter_data(rpcdata, fired=True, might_be_noise=False, isin_time_window=True):
    return rpcdata.query(
        f"fired == {fired} & might_be_noise == {might_be_noise} & isin_time_window == {isin_time_window}"
    )


def extract_height(hvdata, fired=True, might_be_noise=False):
    return filter_data(hvdata, fired, might_be_noise)["height"]


def extract_time_resolution(hvdata, fired=True, might_be_noise=False):
    return filter_data(hvdata, fired, might_be_noise)["time_resolution"]


def extract_charge(hvdata, fired=True, might_be_noise=False):
    return filter_data(hvdata, fired, might_be_noise).groupby("nevent")["charge"].sum()


def extract_cluster_size(hvdata, fired=True, might_be_noise=True):
    return (
        filter_data(hvdata, fired, might_be_noise)
        .groupby("nevent")["cluster_size"]
        .max()
    )


def extract_efficiency(rpcdata):
    return rpcdata.groupby("voltage")["efficiency"].max()


def extract_efficiency_error(rpcdata):
    return rpcdata.groupby("voltage")["efficiency_error"].max()


def extract_streamer_probability(rpcdata):
    return rpcdata.groupby("voltage")["streamer_probability"].max()


def get_main_params(df):
    data = pd.DataFrame()
    for rpc, rpcgroup in df.groupby("rpc"):
        eff = extract_efficiency(rpcgroup)
        eff_error = extract_efficiency_error(rpcgroup)
        prob = extract_streamer_probability(rpcgroup)
        rpcdata = pd.DataFrame()

        for voltage, vgroup in rpcgroup.groupby("voltage"):
            charge = extract_charge(vgroup)
            height = extract_height(vgroup)
            time_resolution = extract_time_resolution(vgroup)
            cluster_size = extract_cluster_size(vgroup)
            rpcdata = rpcdata.append(
                pd.DataFrame(
                    {
                        "rpc": rpc,
                        "hv": voltage,
                        "charge": [charge],
                        "height": [height],
                        "time_resolution": [time_resolution],
                        "cluster_size": [cluster_size],
                    }
                )
            )

        rpcdata["efficiency"] = tuple(eff)
        rpcdata["efficiency_error"] = tuple(eff_error)
        rpcdata["streamer_probability"] = tuple(prob)
        data = data.append(rpcdata)

    return data


def resistance_high_flow(df, column, from_voltage=9200, last_n_points=3):
    """Return the computed resistance from the currents of the last points"""
    sliced_df = df[(df.hv > from_voltage)].iloc[
        -last_n_points:, :
    ]  # take only the last three points
    resistance, _ = np.polyfit(sliced_df.hv, sliced_df[column], 1)
    return 1 / resistance * 1e6


def effective_voltage(df, column, resistance):
    voltages = df.hv - resistance * df[column] * 1e-6
    return voltages


def get_df_hv_eff(dfcurrents, resistances=(4.95e7, 3.86e7)):
    dfcurrents.rename(columns={"HV": "hv"}, inplace=True)
    for num_rpc in [0, 1]:
        rpc = f"RPC{num_rpc}"
        resistance = resistances[num_rpc]
        dfcurrents[f"HVeff{num_rpc}"] = effective_voltage(dfcurrents, rpc, resistance)

    if "RPC0-beam" in dfcurrents.columns:
        currents_melted = dfcurrents.melt(
            id_vars=["hv"], value_vars=["RPC0-beam", "RPC1-beam"], value_name="currents"
        )
    else:
        currents_melted = dfcurrents.melt(
            id_vars=["hv"], value_vars=["RPC0", "RPC1"], value_name="currents"
        )

    hv_melted = dfcurrents.melt(
        id_vars=["hv"], value_vars=["HVeff0", "HVeff1"], value_name="HVeff"
    )

    return pd.DataFrame(
        {
            "hv": currents_melted["hv"],
            "hveff": hv_melted["HVeff"],
            "currents": currents_melted["currents"],
        }
    )


def fit_efficiency(
    hv,
    eff,
    eff_error,
    hv_err=5,
    initial_params=[0.7, 0.01, 10500],
    silent=True,
    voltage_limits=(8400, 12000),
):
    # data to fit
    hv = np.asarray(hv, dtype=float)
    hv_error = np.asarray(np.full_like(hv, hv_err, dtype=float))
    n = hv.size
    eff = np.asarray(eff)
    eff_error = np.asarray(eff_error)

    [effmax, gamma, hv50] = initial_params

    tgraph0 = ROOT.TGraphErrors(n, hv, eff, hv_error, eff_error)
    fit_func = ROOT.TF1("fit_func", "[0] / (1 + TMath::Exp(-[1] * (x - [2])))")
    fit_func.SetParameters(effmax, gamma, hv50)
    fit_func.SetParLimits(0, 0, 1)
    fit_func.SetParLimits(2, *voltage_limits)
    fit_func.SetParNames("effmax", "lambda", "hv50")

    if silent:
        tgraph0.Fit(fit_func, "Q")
    else:
        tgraph0.Fit(fit_func)

    # Save the fit results
    effmax = fit_func.GetParameter(0)
    chi2 = fit_func.GetChisquare() / fit_func.GetNDF() if fit_func.GetNDF() != 0 else -1
    hv50 = fit_func.GetParameter(2)
    gamma = fit_func.GetParameter(1)
    prob = fit_func.GetProb()

    return effmax, gamma, hv50, chi2, prob


def merge_main_with_currents_df(main, hveff_df):
    first_hv_value = main["hv"].min()
    sliced_hveff = hveff_df[hveff_df["hv"] >= first_hv_value].reset_index()
    main["currents"] = sliced_hveff["currents"]
    try:
        main["hveff"] = sliced_hveff["hveff"].astype(int)
    except:
        pass
    return main


def sigmoid_function(hv, effmax, gamma, hv50):
    return effmax / (1 + np.exp(-gamma * (hv - hv50)))


def inverse_sigmoid_function(eff, effmax, gamma, hv50):
    return (gamma * hv50 - np.log((effmax - eff) / eff)) / gamma


def get_hv_efficiency_knee(effmax, gamma, hv50):
    eff_knee = effmax * 0.95  # CMS definition
    hv_knee = inverse_sigmoid_function(eff_knee, effmax, gamma, hv50)
    return hv_knee


def sigmoid_derivative(hv, effmax, gamma, hv50):
    return (
        effmax
        * gamma
        * np.exp(-gamma * (hv - hv50))
        / (np.exp(-gamma * (hv - hv50))) ** 2
    )


def slope(effmax, gamma, hv50):
    m = sigmoid_derivative(hv50, effmax, gamma, hv50)
    x = hv50
    y = sigmoid_function(hv50, effmax, gamma, hv50)
    q = y - m * x
    return m, q


def get_efficiency_info(df, rpc=None, initial_params=[0.9, 0.01, 10500]):
    if rpc:
        df = df[df.rpc == rpc]
    hv = df.voltage.unique()
    eff = df.groupby("voltage")["efficiency"].mean()
    eff_error = df.groupby("voltage")["efficiency_error"].mean()
    streamer_probability = df.groupby("voltage")["streamer_probability"].mean()

    effmax, gamma, hv50, chi2, prob = fit_efficiency(
        hv, eff, eff_error, initial_params=initial_params
    )
    knee = get_hv_efficiency_knee(effmax, gamma, hv50)
    stprob_knee = np.interp(knee, hv, streamer_probability)

    m, q = slope(effmax, gamma, hv50)

    return {
        "effmax": effmax,
        "gamma": gamma,
        "hv50": hv50,
        "chi2": chi2,
        "prob": prob,
        "m": m,
        "q": q,
        "knee": knee,
        "stprob_knee": stprob_knee,
    }


def add_efficiency_fit(df, columns=["hv", "efficiency", "efficiency_error"]):
    finaldf = pd.DataFrame()
    for rpc, rpcgroup in df.groupby("rpc"):
        df = rpcgroup.copy()
        hv, eff, eff_error = df[columns].T.values
        effmax, gamma, hv50, chi2, prob = fit_efficiency(hv, eff, eff_error)
        df["effmax"] = effmax
        df["gamma"] = gamma
        df["hv50"] = hv50
        df["knee"] = get_hv_efficiency_knee(effmax, gamma, hv50)
        df["chi2norm"] = chi2
        df["prob"] = prob
        df["rpc"] = rpc

        finaldf = finaldf.append(df)
    return finaldf


def analyse_run_folder_and_currents(row, configs):
    dfcurrents = read_currents(row)
    df = analyse_run_folder(row["path"], configs)
    main = get_main_params(df)
    main.reset_index(inplace=True)
    hveff_df = get_df_hv_eff(dfcurrents)
    try:
        df_merged = merge_main_with_currents_df(main, hveff_df)
    except:
        pass
    df_merged = add_efficiency_fit(df_merged)

    return df, df_merged, row


def parse_excel_runs(
    filepath, replace_from=None, replace_to=None, append=None, unix=True, engine=None,
):
    df = pd.read_excel(filepath, engine=engine)
    if "place" in df:
        df["path"] = df["place"].astype(str) + "/" + df["path"]
        df["currents"] = df["place"].astype(str) + "/" + df["currents"]
    if unix:
        df["path"] = df.path.str.replace("\\", "/")
        df["currents"] = df.currents.str.replace("\\", "/")

    if replace_from and replace_to:
        df["path"] = df.path.str.replace(replace_from, replace_to)
        df["currents"] = df.currents.str.replace(replace_from, replace_to)

    if append:
        df["path"] = append + df["path"]
        df["currents"] = append + df["currents"]
    df["name"] = df["path"].apply(transform_name)

    return df


def transform_name(path):
    name = "_".join(Path(path).parts[-3:])
    return name


def analyse_and_save_efficiency(df, initial_params, savepath, name):
    rpcs = df.rpc.unique()
    df_eff_info = pd.DataFrame()
    for rpc in rpcs:  # hardcoded
        eff_info = get_efficiency_info(df, rpc=rpc, initial_params=initial_params)
        df_eff_info = df_eff_info.append(pd.DataFrame([{**eff_info, "rpc": rpc}]))

    df_eff_info.to_pickle(savepath / f"{name}_eff.pkl")

    return df_eff_info


def analyse_and_save_lite(
    savepath, configs, row, force=False, force_eff=False, analyse_currents=False
):
    name = transform_name(row["path"])
    skip = True if not pd.isna(row["notes"]) and "no data" in row["notes"] else False

    # Check if data is already existing or not
    if not Path(savepath / f"{name}_lite.pkl").exists() or force:
        # if there is not data or it's force to be analysed again
        if not skip:
            # check if the currents should be analysed
            if analyse_currents:
                df_data, df_avg, row = analyse_run_folder_and_currents(row, configs)
                df_avg.to_pickle(savepath / f"{name}_avg.pkl")
            else:
                df_data = analyse_run_folder(row["path"], configs)

            # save also a lite copy for lighter memory usage (delete data column)
            df_data.to_pickle(savepath / f"{name}_lite.pkl")
            row.to_pickle(savepath / f"{name}_row.pkl")
            pd.DataFrame([configs]).to_pickle(savepath / f"{name}_configs.pkl")
            # Save also info about the efficiency
            df_eff_info = analyse_and_save_efficiency(
                df_data, configs["fit_params"], savepath, name
            )
    else:
        # check if lite has to be loaded
        df_data = pd.read_pickle(savepath / f"{name}_lite.pkl")

    if (
        not Path(savepath / f"{name}_eff.pkl").exists() or force_eff
    ):  # then load the data fit only the efficiency
        df_eff_info = analyse_and_save_efficiency(
            df_data, configs["fit_params"], savepath, name
        )
    else:
        df_eff_info = pd.read_pickle(savepath / f"{name}_eff.pkl")

    return df_data, df_eff_info


def analyse_and_save(
    savepath,
    configs,
    row,
    force=False,
    force_eff=False,
    lite=False,
    analyse_currents=False,
):
    name = transform_name(row["path"])
    skip = True if not pd.isna(row["notes"]) and "no data" in row["notes"] else False

    # Check if data is already existing or not
    if not Path(savepath / f"{name}_data.pkl").exists() or force:
        # if there is not data or it's force to be analysed again
        if not skip:
            # check if the currents should be analysed
            if analyse_currents:
                df_data, df_avg, row = analyse_run_folder_and_currents(row, configs)
                df_avg.to_pickle(savepath / f"{name}_avg.pkl")
            else:
                df_data = analyse_run_folder(row["path"], configs)

            df_data.to_pickle(savepath / f"{name}_data.pkl")
            # save also a lite copy for lighter memory usage (delete data column)
            df_data.drop(columns=["data"]).to_pickle(savepath / f"{name}_lite.pkl")
            if lite:
                df_data = df_data.drop(columns=["data"])
            row.to_pickle(savepath / f"{name}_row.pkl")
            pd.DataFrame([configs]).to_pickle(savepath / f"{name}_configs.pkl")
            # Save also info about the efficiency
            df_eff_info = analyse_and_save_efficiency(
                df_data, configs["fit_params"], savepath, name
            )
    else:
        # check if lite has to be loaded
        if lite:
            df_data = pd.read_pickle(savepath / f"{name}_lite.pkl")
        else:
            df_data = pd.read_pickle(savepath / f"{name}_data.pkl")

    if (
        not Path(savepath / f"{name}_eff.pkl").exists() or force_eff
    ):  # then load the data fit only the efficiency
        df_eff_info = analyse_and_save_efficiency(
            df_data, configs["fit_params"], savepath, name
        )
    else:
        df_eff_info = pd.read_pickle(savepath / f"{name}_eff.pkl")

    return df_data, df_eff_info


def get_corrected_parameters(
    data,
    dfcurrent,
    resistance,
    fit_params=[0.6, 0.01, 9700],
    column="RPC0",
    stprob_column="streamer_probability",
):
    data = data.query("is_detected")
    # Use the set to compute the common hv points between the
    # recorded currents and the recorded efficiency with the digitizer
    set_data_voltage = set(data.voltage)
    set_hv = set(dfcurrent["HV"])
    hv_data = sorted(list(set_hv & set_data_voltage))
    data = data[data.voltage.isin(hv_data)].sort_values(by="voltage")
    dfcurrent = dfcurrent[dfcurrent.HV.isin(hv_data)]
    currents = dfcurrent.sort_values(by="HV")[column].astype(float)

    # Filter based on avalanche / streamer
    data_av = data.query('type == "avalanche"')
    data_st = data.query('type == "streamer"')
    set_av = set(data_av.voltage)
    set_st = set(data_st.voltage)
    hv_data_av = sorted(list(set_hv & set_av))
    hv_data_st = sorted(list(set_hv & set_st))
    dfcurrent_av = dfcurrent[dfcurrent.HV.isin(hv_data_av)]
    currents_av = dfcurrent_av.sort_values(by="HV")[column].astype(float)
    dfcurrent_st = dfcurrent[dfcurrent.HV.isin(hv_data_st)]
    currents_st = dfcurrent_st.sort_values(by="HV")[column].astype(float)

    # Mean parameters extraction for each voltage
    eff = data.groupby("voltage")["efficiency"].mean()
    eff_err = data.groupby("voltage")["efficiency_error"].mean()
    stprob = data.groupby("voltage")[stprob_column].mean()
    mean_charge_av = data_av.groupby("voltage")["charge"].mean()
    mean_charge_st = data_st.groupby("voltage")["charge"].mean()
    mean_height_av = data_av.groupby("voltage")["height"].mean()
    mean_height_st = data_st.groupby("voltage")["height"].mean()
    mean_cluster_size = data.groupby("voltage")["cluster_size"].mean()
    mean_fwhm_av = data_av.groupby("voltage")["fwhm_time"].mean()
    mean_fwhm_st = data_st.groupby("voltage")["fwhm_time"].mean()

    hveff = hv_data - resistance * currents * 1e-6  # uA -> A
    hveff_av = hv_data_av - resistance * currents_av * 1e-6  # uA -> A
    hveff_st = hv_data_st - resistance * currents_st * 1e-6  # uA -> A
    # Fit of efficiency with hveff
    effmax, gamma, hv50, chi2, prob = fit_efficiency(hveff, eff, eff_err, 5, fit_params)
    knee = get_hv_efficiency_knee(effmax, gamma, hv50)
    stprob_knee = np.interp(knee, hveff, stprob)
    stprob_eff = stprob_knee / effmax
    # Interpolation of mean parameters
    charge_knee_av = np.interp(knee, hveff_av, mean_charge_av)
    height_knee_av = np.interp(knee, hveff_av, mean_height_av)
    charge_knee_st = np.interp(knee, hveff_st, mean_charge_st)
    height_knee_st = np.interp(knee, hveff_st, mean_height_st)
    cluster_size_knee = np.interp(knee, hveff, mean_cluster_size)
    fwhm_knee_av = np.interp(knee, hveff_av, mean_fwhm_av)
    fwhm_knee_st = np.interp(knee, hveff_st, mean_fwhm_st)
    current_knee = np.interp(knee, hveff, currents)
    # Return the values in different formats
    single_values = dict(
        effmax=effmax,
        gamma=gamma,
        hv50=hv50,
        knee=knee,
        stprob_knee=stprob_knee,
        stprob_eff=stprob_eff,
        charge_knee_av=charge_knee_av,
        height_knee_av=height_knee_av,
        charge_knee_st=charge_knee_st,
        height_knee_st=height_knee_st,
        cluster_size_knee=cluster_size_knee,
        fwhm_knee_av=fwhm_knee_av,
        fwhm_knee_st=fwhm_knee_st,
        current_knee=current_knee,
    )
    try:
        df = pd.DataFrame(
            {
                "hv": hv_data,
                "hveff": hveff.values,
                "current": currents.values,
                "efficiency": eff.values,
                "efficiency_error": eff_err.values,
                "streamer_probability": stprob.values,
                "mean_charge_av": mean_charge_av.values,
                "mean_charge_st": mean_charge_st.values,
                "mean_height_av": mean_height_av.values,
                "mean_height_st": mean_height_st.values,
                "mean_cluster_size": mean_cluster_size.values,
                "mean_fwhm_av": mean_fwhm_av.values,
                "mean_fwhm_st": mean_fwhm_st.values,
            }
        ).sort_values(by="hveff")
    except ValueError as e:
        df = {
            "hv": hv_data,
            "hveff": hveff.values,
            "current": currents.values,
            "efficiency": eff.values,
            "efficiency_error": eff_err.values,
            "streamer_probability": stprob.values,
            "mean_charge_av": mean_charge_av.values,
            "mean_charge_st": mean_charge_st.values,
            "mean_height_av": mean_height_av.values,
            "mean_height_st": mean_height_st.values,
            "mean_cluster_size": mean_cluster_size.values,
            "mean_fwhm_av": mean_fwhm_av.values,
            "mean_fwhm_st": mean_fwhm_st.values,
        }

    return single_values, df


def construct_hv_folders_dataframe(row):
    run_path = Path(row.path)
    path = run_path / "MIX0"
    # find all the hv folders
    hv_folders = list(path.glob("HV*"))

    parameters_path = tuple(run_path.glob("**/parameters.dat"))[0]
    dfpar = pd.read_csv(parameters_path, header=None, delimiter="\t")
    dfpar["hv"] = dfpar.iloc[:, 1].apply(lambda x: f"HV{x}")
    dfpar.rename(columns={2: "voltage"}, inplace=True)
    hv_values = dfpar[["hv", "voltage"]].copy()
    hv_values["path"] = hv_folders

    return hv_values


def calc_peaks_counts(
    row,
    window=51,
    pol=3,
    height=7,
    distance=200,
    print_log=True,
    delete_window=None,
    header_length=520,
    exclude=None,
):
    hv_values = construct_hv_folders_dataframe(row)
    # general dataframe holdings all the results
    nevents = 900  # first guess
    df = pd.DataFrame()
    if (
        delete_window
    ):  # create an array of indices used as a mask to delete the parts in which a muon is expected
        excluded_ix = []
        start = delete_window[0]
        stop = delete_window[1]
        for item in zip(
            header_length * np.arange(nevents) + start,
            header_length * np.arange(nevents) + stop,
        ):
            excluded_ix = np.append(excluded_ix, np.arange(item[0], item[1]))
        excluded_ix = excluded_ix.astype(int)
    for ix, voltage_row in hv_values.iterrows():
        voltage = voltage_row.voltage
        path = voltage_row.path
        for wavefile in path.glob("wave*.txt"):
            skip = False
            strip = int(wavefile.stem.strip("wave"))
            if strip == 0:
                continue  # skip ch0 because is reserved to the trigger
            # check if there are strips to exclue
            if exclude:
                for exclude_rpc, exclude_strip in exclude.items():
                    if strip == exclude_strip and rpc == exclude_rpc:
                        skip = True
            if skip:
                continue
            rpc = 0 if strip <= 7 else 1
            dfwave = pd.read_csv(wavefile, header=None, names=["wave"])
            wave = dfwave.wave.values
            nevents_wave = wave.size / header_length
            if delete_window and nevents_wave != nevents:
                for item in zip(
                    header_length * np.arange(nevents) + start,
                    header_length * np.arange(nevents) + stop,
                ):
                    excluded_ix = np.append(excluded_ix, np.arange(item[0], item[1]))
                    excluded_ix = excluded_ix.astype(int)

            if delete_window:
                # delete the part of the wave corresponding to the indices in excluded_ix
                wave = np.delete(wave, excluded_ix)

            length = wave.size
            # reverse the signal
            wave = -wave + wave.mean()
            filtered = savgol_filter(wave, window, pol)
            peaks, _ = find_peaks(filtered, height=height, distance=distance)
            counts = peaks.size
            df = df.append(
                pd.Series(
                    {
                        "rpc": rpc,
                        "strip": strip,
                        "counts": counts,
                        "path": path,
                        "length": length,
                        "voltage": voltage,
                    }
                ),
                ignore_index=True,
            )
            if print_log:
                print(strip, sep=" ", end=" ")
        if print_log:
            print(path)

    return df


def calc_peaks_rate(df):
    df_rate = df.groupby(["rpc", "voltage"])[["counts", "length"]].sum().reset_index()
    # this line counts the number of fired strips by checking the unique strips and counting them
    df_rate["num_strips"] = (
        df.groupby(["rpc", "voltage"])
        .agg(lambda x: x.unique().size)
        .reset_index()["strip"]
        .astype(int)
    )
    df_rate["strip_length"] = df_rate["rpc"].apply(lambda x: 80 if x == 0 else 140)
    df_rate["strip_width"] = 2
    df_rate["time_resolution"] = 2e-9
    df_rate["rate"] = df_rate["counts"] / (
        df_rate["strip_length"]
        * df_rate["strip_width"]
        * df_rate["length"]
        * df_rate["time_resolution"]
    )
    return df_rate


def calc_rate(
    row,
    window=51,
    pol=3,
    height=7,
    distance=200,
    print_log=True,
    delete_window=None,
    header_length=520,
    exclude=None,
):
    df = calc_peaks_counts(
        row,
        window,
        pol,
        height,
        distance,
        print_log,
        delete_window,
        header_length,
        exclude,
    )
    df_rate = calc_peaks_rate(df)
    return df_rate


def insert_line_excel(excel_file_path, date, run_path, currents_path, **kwargs):
    df = pd.read_excel(excel_file_path)
    row = pd.Series(
        {
            "date": pd.to_datetime(date),
            "path": run_path,
            "currents": currents_path,
            **kwargs,
        },
        ignore_index=True,
    )
    df = df.append(row)
    df.to_excel(excel_file_path)


def find_row(excel_file_path, name, append=None, replace_from=None, replace_to=None):
    df = parse_excel_runs(excel_file_path)
    row = df[df.name == name]
    if row.size == 0:
        return None

    return row.iloc[0]


def estimate_geometrical_efficiency(detection_rpc, trigger_rpc):
    detection_events = set(detection_rpc.query("is_detected").nevent.unique())
    trigger_events = set(trigger_rpc.query("is_detected").nevent.unique())
    efficiency = detection_rpc["efficiency"].mean()
    if not detection_events or not trigger_events:
        return efficiency, 1
    geometrical_efficiency = len(detection_events & trigger_events) / len(
        trigger_events
    )
    return efficiency, geometrical_efficiency


def get_df_geometrical_efficiency(df, detection_rpc_column=0, trigger_rpc_column=1):
    df_result = pd.DataFrame()
    for voltage, group in df.groupby("voltage"):
        detection_rpc = group[group.rpc == detection_rpc_column]
        trigger_rpc = group[group.rpc == trigger_rpc_column]
        result = estimate_geometrical_efficiency(detection_rpc, trigger_rpc)
        df_result = df_result.append(pd.Series([voltage, *result]), ignore_index=True)

    return df_result.rename(columns=dict(enumerate(["voltage", "eff", "eff_geom"])))


def geom_mean_ratio(
    df, detection_rpc_column=0, trigger_rpc_column=1, last_num_points=3
):
    results = get_df_geometrical_efficiency(
        df, detection_rpc_column, trigger_rpc_column
    )
    results["ratio"] = results["eff_geom"] / results["eff"]
    return results.ratio[-last_num_points:].mean()


def get_data(path, voltage, rpc, strip, event, record_length=520):
    path = Path(path)
    parameters_path = tuple(path.glob("**/parameters.dat")) or tuple(
        path.glob("**/Parameters.dat")
    )
    parameters_path = parameters_path[0]

    dfpar = pd.read_csv(parameters_path, header=None, delimiter="\t")
    dfpar["hv"] = dfpar.iloc[:, 1].apply(lambda x: f"HV{x}")
    dfpar.rename(columns={2: "voltage"}, inplace=True)
    hvname = dfpar[dfpar.voltage == voltage]["hv"].iloc[0]
    hvpath = path / "MIX0" / hvname

    wavenum = strip + rpc * 7
    wavepath = hvpath / f"wave{wavenum}.txt"
    dfwave = pd.read_csv(wavepath, header=None, names=["wave"])
    data = dfwave["wave"].values.reshape((-1, record_length))[event, :]

    return data
