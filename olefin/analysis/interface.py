from abc import ABCMeta, abstractmethod


class BaseAnalysis(metaclass=ABCMeta):
    """Abstract class defining the funamental piece of analysis.

    The class should implement a constructor and a run() method. The constructor
    should be used to set the necessary values to run the analysis. In 
    particular, the .configs attribute and the 
    """

    @abstractmethod
    def __init__(self):
        """Construct of the analysis class. This is usual the place where
        the configs are passed as an attribute of the class"""
        pass

    @abstractmethod
    def run(self):
        """Method that will run the analysis. It should also set the .data
        attribute of the class"""
        pass

    @property
    @abstractmethod
    def data(self):
        """This will represent the data resulted from the analysis instance"""
        pass

    @property
    @abstractmethod
    def config(self):
        """Configs represent all the data necessary to run the analysis"""
        pass
