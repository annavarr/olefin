import numba as nb
import numpy as np
import typing as t
from pathlib import Path
import re
import pandas as pd
from scipy.special import erfc
import scipy.optimize as so
from box import Box
import yaml
import logging
import olefin
import scipy.interpolate as si
import psycopg2
from psycopg2.extras import Json
from psycopg2.extras import execute_values
from psycopg2.extensions import register_adapter
import datetime


logger = logging.getLogger(__name__)


class SignalTypes:
    uncategorized = -2
    false_positive = -1
    noise = 0
    avalanche = 1
    extra_charge = 2
    streamer = 3


gwp_gases = Box(
    {
        "HFO": 7,
        "R134A": 1430,
        "R1234ZE": 7,
        "SF6": 22800,
        "CO2": 1,
        "AR": 0,
        "N2": 0,
        "HE": 0,
        "IC4H10": 3.3,
        "NOVEC5110": 0,
        "NOVEC4710": 7000,
        "C4F8O": 8000,
        "HFO1224YD": 0,
        "R1224YD": 0,
        "CF3I": 0,
        "O2": 0,
        "N2O": 280,
        "R1233ZD": 1,
        "NE": 0,
        "R32": 675,
        "R245FA": 1030,
        "R152A": 124,
    }
)

molecular_weight_gases = Box(
    {
        "HFO": 114,  # g / mol
        "R134A": 102.03,
        "R1234ZE": 114,
        "SF6": 146.06,
        "CO2": 44,
        "AR": 40,
        "N2": 28,
        "HE": 4,
        "IC4H10": 58.12,
        "NOVEC5110": 266,
        "NOVEC4710": 195,
        "C4F8O": 216,
        "HFO1224YD": 148.5,
        "R1224YD": 148.5,
        "CF3I": 195.9,
        "O2": 32,
        "N2O": 44,
        "R1233ZD": 130.5,
        "NE": 20,
        "R32": 52.02,
        "R245FA": 134,
        "R152A": 66.05,
    }
)


@nb.njit
def nb_expander(x):
    if x.ndim == 1:
        return np.expand_dims(x, axis=0)
    else:
        return x


@nb.njit
def clusize_algorithm(event_arr):  # pragma: no cover
    """This method uses numba and its x100 time
    faster than the vectorized method"""
    if event_arr.ndim == 1:
        res = np.empty(1, dtype=np.int16)
    else:
        res = np.empty(event_arr.shape[0], dtype=np.int16)
    events = nb_expander(event_arr)
    for ix in range(len(events)):
        max_cluster = 0
        temp_counter = 0
        event = events[ix]
        for ix_strip in range(len(event)):
            if event[ix_strip] == 1:
                temp_counter += 1
                if temp_counter > max_cluster:
                    max_cluster = temp_counter
            else:
                temp_counter = 0
        res[ix] = max_cluster

    return res


def clusize(event_arr):
    res = clusize_algorithm(event_arr)
    if event_arr.ndim == 1:
        return res[0]
    return res


def get_acquisition_analysis(
    config,
    folder,
    acquisition_class,
    event_class,
    signal_class,
):
    acquisition = acquisition_class.from_folder(
        config=config, folder=folder, event_class=event_class, signal_class=signal_class
    )
    acquisition.run()
    return acquisition


def get_rate_acquisition_analysis(
    config,
    folder,
    acquisition_class,
    signal_class,
):
    acquisition = acquisition_class.from_folder(
        config=config, folder=folder, signal_class=signal_class
    )
    acquisition.run()
    return acquisition


def parse_currents_autogen(
    filepath: t.Union[str, Path],
    header: t.Optional[list] = None,
    column_map: t.Optional[dict] = None,
    multi_current: bool = False,
    currents_columns: t.Union[
        str, "re.Pattern"
    ] = r"(?:(?:.*RPC)?(.*?))(?:(?:-(beam))|(?:_(std))|(?:$))",
    index_col: bool = False,
) -> pd.DataFrame:
    """Parse auto generated currents from DAQ into a pandas dataframe.

    The currents are usually created and saved by the acquisition program.
    The format of the data source is the following:

    | index (optional) | HV   | Run    | RPC0 | RPC0-beam | RPC1 |
    |                0 | 2000 | Abs232 | 0.10 |      1.45 | 0.22 |

    Where index is created by pandas at the time of writing the file, HV is
    the applied voltage, Run is the name of the run and there are multiple
    columns depending on the number of RPC under scan

    During the testbeam it is possible that for a single RPC two currents are
    recorded: one of the background (gammas or source off) and one with muon
    beam. It's possible to configure the parameters so that the beam current
    is also taken into account.

    The returned dataframe will be a flat table having as index the voltage and
    RPC and 1 or 2 columns depending if the beam current has been recorded.

    An example structure looks like this:

    | RPC | HV   | current | beam_current |
    |  0  | 9800 |    0.13 |         1.30 |

    Parameters
    ----------
    filepath:
        the path to the data source. It could be either an .xlsx or a .csv
        file, but it should respect the tabular structure described above.
    column_map:
        a dict containing the name map for the columns. It will use pandas
        .rename() function to rename the columns from the parsed DataFrame
    currents_column:
        a regex indicating which columns contains the current.
        The name of the column should also contain the name of the RPC
    index_col:
        the same as `index_col` in the pandas read_csv() function

    """
    filepath = Path(filepath)
    # Parse row file into a dataframe
    if filepath.suffix == ".xlsx":
        df = pd.read_excel(filepath, index_col=index_col, engine="openpyxl")
    elif filepath.suffix == ".csv":
        df = pd.read_csv(filepath, index_col=index_col)
    else:
        raise ValueError(
            "Only .xlsx and .csv files supported" +
            f" but provided one was {filepath.suffix}"
        )
    # Delete the 'Unnamed 0' column in case is present: this happens when
    # df is saved with index and is read without telling explictly to use
    # index_col = 0
    if "Unnamed: 0" in df.columns:
        df = df.drop(columns="Unnamed: 0")
    # Melt dataframe so that the columns become rows values
    # https://pandas.pydata.org/pandas-docs/stable/user_guide/reshaping.html#reshaping-by-melt
    melted = df.melt(id_vars=["run", "HV"])
    # Apply the regex on the 'variable' column to extract the RPC number
    # the current type (either NaN or beam for now)
    dfa = melted["variable"].str.extract(currents_columns)
    # The regex is passed so that group 0 is the rpc number,
    # group 1 is the type of current (standard or beam)
    # group 2 is the type of statitisc (mean or std to be used as central
    # value and error)
    # The resulting dataframe has 3 columns for the 3 groups of the regex
    # We extract only col 0 and col 2 and fillNa col 2
    # with the 'standard' label
    dfa = dfa.rename({0: "RPC", 1: "currents_type", 2: "currents_stat"}, axis=1)
    try:
        dfa["RPC"] = dfa.RPC.astype(int)
    except Exception:
        logging.debug("Could not parse df currents RPC name as integer")
    dfa = dfa.fillna({"currents_type": "standard", "currents_stat": "mean"})
    tot = pd.concat([melted, dfa], axis=1)
    tot = tot.pivot(
        index=["run", "RPC", "HV"],
        columns=["currents_type", "currents_stat"],
        values="value",
    )
    # Flatten the multi column level by joining column names with a '_'
    tot.columns = ["_".join(col).rstrip("_") for col in tot.columns.values]
    # Add prefix currents for better readability
    tot = tot.add_prefix("currents_")

    return tot


def gaussian_model(params, x):
    """Gaussian model definition for ODR fit."""
    return params[0] * np.exp(
        -0.5 * ((x - params[1]) / params[2]) ** 2
    )  # pragma: no cover


def gaussian_model_2(x, mu, c, sigma):
    """Gaussian model in a different parameter for for curve fit"""
    return c * np.exp(-0.5 * ((x - mu) / sigma) ** 2)  # pragma: no cover


def expo_gaussian_model(x, h, m, s, t):
    """Model for an exponentially modified gaussian.

    The definition is taken from the first formula of
    'Alternative form of computations' from
    https://en.wikipedia.org/wiki/Exponentially_modified_Gaussian_distribution

    Parameters definition:
    h = height
    m = mean
    s = sigma
    t = tau
    """
    return (  # pragma: no cover
        h *
        s /
        t *
        np.sqrt(np.pi / 2) *
        np.exp(1 / 2 * (s / t) ** 2 - (x - m) / t) *
        erfc(1 / np.sqrt(2) * (s / t - (x - m) / s))
    )


def fit_model(
    x: np.ndarray,
    y: np.ndarray,
    model: t.Callable,
    initial_params: t.List,
    yerr=None,
    bounds=(-np.inf, np.inf),
):
    """
    Utility function to fit data to a model.

    Model in this case should be a function of type func(params, x) where
    params is an array of parameters to fit. ODR is used to fit the data.

    Parameters
    ----------
    x : np.ndarray
        the x values
    y : np.ndarray
        the y values
    model : callable
        a function with arguments ``f(params, x)`` where x is the x data
        and params a list of parameters to be fitted
    initial_params: list or np.ndarray
        an initial guess of parameters
    xerr : np.ndarray optional
        the errors on x values
    yerr : np.ndarray optional
        the errors on y values

    Returns
    -------
    tuple of list
        returns fitted parameters, errors on fitted parametrs and reduced
        chi square
    """
    popt, errors, reduced_chi2 = (
        np.full_like(initial_params, np.nan),
        np.full_like(initial_params, np.nan),
        np.nan,
    )
    try:
        popt, pcov = so.curve_fit(
            model,
            x,
            y,
            sigma=yerr,
            absolute_sigma=True,
            p0=initial_params,
            bounds=bounds,
        )
        errors = np.sqrt(np.diagonal(pcov))
        dof = len(x) - len(popt)
        reduced_chi2 = ((y - model(x, *popt)) ** 2).sum() / dof
    except Exception as e:
        logging.debug(f"Fit returned nan values. x: {x}, y: {y}")
        logging.debug(e)
    return popt, errors, reduced_chi2


def config_from_path(filepath: t.Union[str, Path]) -> Box:
    """Parse a .yml file into a config object.

    Parameters
    ----------
    filepath:
        the path to the config file

    Raises
    ------
    ValueError
        if the file extension is not .yml or .yaml
    """
    filepath = Path(filepath)
    if filepath.suffix in [".yml", ".yaml"]:
        with open(filepath, "r") as f:
            config = Box(yaml.load(f, Loader=yaml.Loader), default_box=True)
            config.config_path = filepath
        return config
    else:
        raise ValueError(
            "File extension is not .yml or " + f".yaml but {filepath.suffix}"
        )


def get_signals(queried_df, config, run_path, repeating_variable="nevent"):
    """Return waveform signals from a signal or event dataframe.

    The function is similar to the bound method of `RunAnalysis.get_signals()`
    except that a config object containing the `mix_folder`, `strip_map` and
    `parameter_map` must be provided. It also requires a `run_path` to build
    the lookup directory.

    Parameters
    ----------
    queried_df: pd.DataFrame
        a signal or event dataframe with 'folder_num' and 'rpc' either in the
        index or in the columns
    config: olefin.config
        a config object with the the properties:
            - `config.run.mix_folder`
            - `config.acquisition.strip_map`
            - `config.run.parameter_map`
    run_path: str or pathlib.Path
        a string indicating the base run path (e.g. 'Cosmics0', 'Cosmics1', )
    repeating_variable: str
        the string representing the name of the column of
        events in the dataframe

    Returns
    -------
    pd.DataFrame
        A dataframe containing:
        The return dataframe consists of the following columns:
        - time_sample: the sample (from 0 to record_length)
        - nevent or event: the number of the event
        - value: the adc value from the waveform
        - rpc
        - voltage_app
        - strip
    """
    resetted = queried_df.reset_index()
    variables = ["folder_num", "rpc"]
    mix_folder = config.run.get("mix_folder", "MIX0")
    strip_map = config.acquisition.strip_map
    parameter_map = config.run.parameter_map

    dftot = []
    for index, values in resetted.groupby(variables):
        nevents = values[repeating_variable].unique()
        combination = dict(zip(variables, index))
        wavenums = [k for k, v in strip_map.items() if v.rpc == combination["rpc"]]
        hv_path = Path(run_path) / mix_folder / f"HV{combination['folder_num']}"

        for wavenum in wavenums:
            wavepath = hv_path / f"{wavenum}.txt"
            signal_analysis = olefin.from_wavefile(config, wavepath)
            events_data = signal_analysis.get_events(nevents)
            df = (
                pd.DataFrame(events_data, index=nevents)
                .T.stack()
                .reset_index()
                .rename(
                    columns={"level_1": "nevent", "level_0": "time_sample", 0: "value"}
                )
            )
            df["rpc"] = combination["rpc"]
            df["voltage_app"] = parameter_map.get(combination["folder_num"], np.nan)
            df["folder_num"] = combination["folder_num"]
            df["strip"] = strip_map[wavenum].strip
            dftot.append(df)
    dftot = pd.concat(dftot).set_index(["folder_num", "rpc", "nevent", "strip"])
    return dftot


def interpolate(x, y, x_interpolation, fill_value="extrapolate", kind="linear"):
    """Interpolate a point from a set of x-y coordinates.

    The function uses scipy.interp1d to compute the interpolation at
    a x_interpolation point."""
    interpolation_function = si.interp1d(x, y, fill_value=fill_value, kind=kind)
    y_interpolation = interpolation_function(x_interpolation)

    return y_interpolation


def get_run_name(config):
    """Return the run name given a config object.

    The run name is typically indicated by the folder name containing
    the run. (e.g. SourceOffBeamOn, Cosmics0, etc.)"""
    return Path(config.run.path).parts[-1]


def get_run_date(config):
    """Return the date of the run.

    The date of the run is obtained by looking at its folder structure:
    .../20220101/no-header-512/Cosmics..
    """
    return datetime.datetime.strptime(
        Path(config.run.path).parts[-3], "%Y%m%d"
    ).isoformat()


def get_gas_label(config):
    """Return a string describing the gas mixture used.

    The string is composed in the following way:
    <gas1>/<gas2>/...  <percentage1>/<percentage2>/...
    the order is set by the highest concentration to the lowest"""
    gas_mixture = dict(
        sorted(config.run.gas_mixture.items(), key=lambda item: item[1], reverse=True)
    )
    names = [i.upper() for i in gas_mixture.keys()]
    values = [f"{i:.4g}" for i in gas_mixture.values()]
    label = "/".join(names) + " " + "/".join(values)
    return label


def store_run_to_db(
    run_analysis,
    dsn,
    columns_renaming={
        "time_res": "time_resolution",
        "time_res_error": "time_resolution_error",
        "time_res_chi2": "time_resolution_chi2",
        "effmax_err": "effmax_error",
        "gamma_err": "gamma_error",
        "hv50_err": "hv50_error",
        "count": "gamma_count",
        "count_error": "gamma_count_error",
    },
    columns_renaming_event={
        "event_charge": "charge",
        "event_height": "height",
        "event_type": "type",
        "time_res_chi2": "time_resolution_chi2",
        "nevent": "trigger_id",
    },
):
    """Store run analysis object to PostgreSQL database.

    This utility function stores run, acquisition and event data to
    a preconfigured Database instance. The data is structured so that each
    run row is unique. Each acquisition row has a 'run_id' column that uniquely
    references the run and each event row has a 'acquisition_id' column that
    uniquely references the acquisition point. If the data is already existing
    it is overwritten with the data contained in the run_analysis object.

    Parameters
    ----------
    run_analysis: olefin.analysis.run.RunAnalysis
        The object returned when performing a full analysis. The object
        must have `acquisition_data` and `event_data` attributes (usually from `keep_data=True`)
    dsn: str
        The data source name to connect to the database. This value is typically in the format
        `host=<db>.cern.ch user=<str> password=<str> port=<int> dbname=<str> sslmode=disable`
    columns_renaming: dict
        A dictionary where the keys are the name of the acquisition columns of the run analysis
        object and the values are the name of the columns in the database
    columns_renaming_event: dict
        A dictionary where the keys are the name of the event columns of the run analysis
        object and the values are the name of the columns in the database


    Returns
    -------
    None

    """
    register_adapter(dict, Json)

    r = run_analysis.data.reset_index().rename(columns=columns_renaming)
    r["gas_mixture"] = get_gas_label(run_analysis.config)
    r["name"] = get_run_name(run_analysis.config)
    r["date"] = get_run_date(run_analysis.config)
    r["folder_path"] = run_analysis.config.run.path
    r["gwp"] = run_analysis.mixture_gwp
    r["config"] = [Json(run_analysis.config.to_dict())] * len(r)

    a = run_analysis.acquisition_data.reset_index().rename(columns=columns_renaming)
    a = a.drop(columns=["run_name"])

    conn = psycopg2.connect(dsn)
    try:
        cur = conn.cursor()
        cols = ",".join(f"%({i})s" for i in r.columns)
        cols2 = ",".join(r.columns)
        cols_excluded = ",".join(f" EXCLUDED.{i}" for i in r.columns)
        sql = f"""INSERT INTO runs ({cols2}) VALUES %s
        ON CONFLICT (rpc, folder_path) DO UPDATE SET
        ({cols2}) = ({cols_excluded})
        RETURNING id;
        """
        res = execute_values(
            cur, sql, r.to_dict(orient="records"), template=f"({cols})", fetch=True
        )
        logging.info(res)

        ids = cur.fetchall()

        for rpc, group in a.groupby("rpc", group_keys=False):
            group = group.drop(columns=["rpc", "index"]).copy()
            cur.execute(
                "SELECT id from runs WHERE rpc = %s AND folder_path = %s",
                (rpc, r.folder_path.iloc[0]),
            )
            run_id = cur.fetchone()[0]
            group["run_id"] = run_id
            cols = ",".join(group.columns)
            cols_named = ",".join(f"%({i})s" for i in group.columns)
            cols_excluded = ",".join(f" EXCLUDED.{i}" for i in group.columns)
            sql = f"""INSERT INTO acquisitions ({cols}) VALUES %s
            ON CONFLICT (run_id, voltage_app) DO UPDATE SET
            ({cols}) = ({cols_excluded})
            RETURNING id;
            """
            execute_values(
                cur, sql, group.to_dict(orient="records"), template=f"({cols_named})"
            )
            ids = cur.fetchall()
            group["id"] = ids
            group = group.explode("id").astype({"id": int}).set_index(["run_id", "id"])

            event_data = run_analysis.event_data.reset_index().rename(
                columns=columns_renaming_event
            )
            for (run_id, acquisition_id), row in group.iterrows():
                subset = (
                    event_data.query("rpc == @rpc and voltage_app == @row.voltage_app")
                    .copy()
                    .drop(columns=["folder_num", "rpc", "voltage", "voltage_app"])
                )
                subset["acquisition_id"] = acquisition_id
                cols = ",".join(subset.columns)
                cols_named = ",".join(f"%({i})s" for i in subset.columns)
                cols_excluded = ",".join(f" EXCLUDED.{i}" for i in subset.columns)
                sql = f"""
                INSERT INTO events ({cols}) VALUES %s
                ON CONFLICT (trigger_id, acquisition_id) DO UPDATE SET
                ({cols}) = ({cols_excluded})
                """
                execute_values(
                    cur,
                    sql,
                    subset.to_dict(orient="records"),
                    template=f"({cols_named})",
                )
        conn.commit()
    except Exception as e:
        logging.exception(e)
    finally:
        conn.close()
