import olefin
from olefin.analysis.interface import BaseAnalysis
import numpy as np
import pandas as pd
import typing as t
from pathlib import Path
import logging
import matplotlib.pyplot as plt


logger = logging.getLogger(__name__)
logging.basicConfig(
    format=(
        "%(asctime)s - %(name)s - %(levelname)s - %(funcName)s - %(lineno)s -"
        " %(message)s"
    ),
)


class EventAnalysis(BaseAnalysis):
    """Analysis at event level

    An Event is a defined as the simultaneous captured signal from different
    strips of the same RPC. This means that one event will contains features
    from different channels of the digitizer so the class requires a raw_data
    format in the shape of a pandas DataFrame.

    The raw_data should be in the shape (n_folders*n_rpc*n_events, n_features*n_strips)
    The dataframe should be made with a multi index and multi column, where
    the multi index is (folder, rpc, event) and multi column (feature, strip)

    The .run() method returns a dataframe with features computed over the event,
    i.e. over the n strips of an RPC. The returned dataframe will be stored in
    the .data attribute and will be in the shape (n_folders*n_rpc*n_events, n_features)
    with a multi index (folder, rpc, event) and no multi columns.

    Attributes
    ----------
    data : pd.DataFrame
        the returned data from the .run() method
    config : olefin.config
        the global configuration objec
    """

    data = None
    config = None

    def __init__(self, config: "olefin.config", raw_data):
        self.config = config
        self.indata = raw_data  # indata = input data
        self.logger = logging.getLogger(__name__)

    @classmethod
    def from_folder(
        cls, config, folder: t.Union[str, Path], signal_class=olefin.SignalAnalysis
    ):
        """
        Create an event analysis class from a folder of wavefiles.

        This method will detect all the wavefiles in the folder and for each
        wavefile will create and run an analysis class that can be passed as
        an argument to the function.
        config.acquisition.strip_map is used to assign each run analysis to
        and rpc and its strip.
        At the end an EventClass is created with .data attribute set from the
        computed dataframe.

        Arguments
        ---------
        config : olefin.config
            the global configuration object
        folder : str or Path
            the path to the folder containing the wavefiles
        signal_class : class of type BaseAnalysis
            the class over which to use the .run() method

        Returns
        -------
        EventAnalysis
            the instantiated class
        """
        res = []
        folder = Path(folder)
        folder_num = int(folder.stem.strip("HV"))

        # Merge and update config from base configs
        parsed_config = olefin.config.copy()
        parsed_config.merge_update(config)

        for wave_name, values in parsed_config.acquisition.strip_map.items():
            wavefile = folder / f"{wave_name}.txt"
            wave_num = int(wave_name.strip("wave"))
            rpc, strip, _ = (
                values["rpc"],
                values["strip"],
                values.get("resistance"),
            )
            if not wavefile.exists():
                logger.warning(
                    f"{wave_name} present in strip_map but not found in folder {folder}"
                )
            signal_analysis = signal_class.from_wavefile(parsed_config, wavefile)
            df = signal_analysis.run()
            df = df.assign(
                folder_num=folder_num,
                wave_num=wave_num,
                rpc=rpc,
                strip=strip,
                event=df.index,
            )
            res.append(df)
        df = pd.concat(res)
        df = df.pivot_table(index=["folder_num", "rpc", "event"], columns="strip")
        # Rename columns so that every level has a name
        df.columns.names = ["feature", "strip"]
        return cls(parsed_config, df)

    def calc_features(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Calculate features on a event analysis class.

        This function will appply aggregated function on the data provided.
        The data should be a pandas dataframe with multicolumn index of the shape
        (feature, strip). The functions applied will run over each row of the
        dataframe and compute global strip features.

        The list of features computed by this function are:
        - event_charge : the sum of collected charge by all the strips
        - cluster_size : the biggest cluster of fired events
        - event_type : the type of event (similar to signal but the biggest found)
        - is_detected : if the event correspond to a detection of a particle
        - event_tot : the maximum time over threshold of all the signals
        - time_peak : the minimum time over which a peak is found

        Returns
        -------
        pd.DataFrame
            a dataframe with the original index passed by data and a number
            of columns corresponding to the number of computed features
        """
        df = data
        event_charge = df.charge.sum(axis=1)
        cluster_size = (
            df.fired.dropna()
            .astype(bool)
            .astype(bool)
            .apply(lambda x: olefin.analysis.utils.clusize(x.values), axis=1)
        )
        # FIXME .astype(bool) is repetead, that can be removed and maybe this can be optimized
        event_type = df.signal_type.max(axis=1)
        detected_signals = (df.signal_type > 0) & (df.fired)
        is_detected = (detected_signals).any(axis=1).astype(bool)
        event_tot = df[detected_signals].tot.max(axis=1)
        time_peak = df[detected_signals].time_peak.min(axis=1)

        return pd.DataFrame(
            {
                "event_charge": event_charge,
                "cluster_size": cluster_size,
                "event_type": event_type,
                "is_detected": is_detected,
                "tot": event_tot,
                "time_peak": time_peak,
            }
        )

    def run(self):
        """
        Run the analysis on the class. This method will run compute the
        features and set the result to the .data attribute
        """
        self.data = self.calc_features(self.indata)
        return self.data


class EPDTEventAnalysis(BaseAnalysis):
    """Analysis at event level

    An Event is a defined as the simultaneous captured signal from different
    strips of the same RPC. This means that one event will contains features
    from different channels of the digitizer so the class requires a raw_data
    format in the shape of a pandas DataFrame.

    The raw_data should be in the shape (n_folders*n_rpc*n_events, n_features*n_strips)
    The dataframe should be made with a multi index and multi column, where
    the multi index is (folder, rpc, event) and multi column (feature, strip)

    The .run() method returns a dataframe with features computed over the event,
    i.e. over the n strips of an RPC. The returned dataframe will be stored in
    the .data attribute and will be in the shape (n_folders*n_rpc*n_events, n_features)
    with a multi index (folder, rpc, event) and no multi columns.

    Attributes
    ----------
    data : pd.DataFrame
        the returned data from the .run() method
    config : olefin.config
        the global configuration objec
    """

    data = None
    config = None

    def __init__(self, config: "olefin.config", raw_data):
        self.config = config
        self.indata = raw_data  # indata = input data
        self.logger = logging.getLogger(__name__)

    @classmethod
    def from_folder(
        cls,
        config,
        folder: t.Union[str, Path],
        signal_class=olefin.EPDTSignalAnalysis,
    ):
        """
        Create an event analysis class from a folder of wavefiles.

        This method will detect all the wavefiles in the folder and for each
        wavefile will create and run an analysis class that can be passed as
        an argument to the function.
        config.acquisition.strip_map is used to assign each run analysis to
        and rpc and its strip.
        At the end an EventClass is created with .data attribute set from the
        computed dataframe.

        Arguments
        ---------
        config : olefin.config
            the global configuration object
        folder : str or Path
            the path to the folder containing the wavefiles
        signal_class : class of type BaseAnalysis
            the class over which to use the .run() method

        Returns
        -------
        EventAnalysis
            the instantiated class
        """
        res = []
        folder = Path(folder)
        folder_num = int(folder.stem.strip("HV"))

        # Merge and update config from base configs
        parsed_config = olefin.config.copy()
        parsed_config.merge_update(config)

        use_trigger_times = config.acquisition.trigger and isinstance(
            parsed_config.acquisition.trigger, str
        )
        # Check if trigger is enabled and run a trigger analysis in case it is
        if use_trigger_times:
            wave_name = f"{parsed_config.acquisition.trigger}.txt"
            trigger_analysis = olefin.TriggerAnalysis.from_wavefile(
                parsed_config, folder / wave_name
            )
            trigger_analysis.run()
            trigger_times = trigger_analysis.data.times
        else:
            trigger_times = None
        for wave_name, values in parsed_config.acquisition.strip_map.items():
            wavefile = folder / f"{wave_name}.txt"
            wave_num = int(wave_name.strip("wave"))
            rpc, strip, resistance = (
                values["rpc"],
                values["strip"],
                values.get("resistance"),
            )
            if not wavefile.exists():
                logger.warning(
                    f"{wave_name} present in strip_map but not found in folder {folder}"
                )
            signal_analysis = signal_class.from_wavefile(parsed_config, wavefile)
            df = signal_analysis.run(trigger_times=trigger_times, resistance=resistance)
            df = df.assign(
                folder_num=folder_num,
                wave_num=wave_num,
                rpc=rpc,
                strip=strip,
                nevent=df.index,
            )
            res.append(df)
        df = pd.concat(res)
        df = df.pivot_table(index=["folder_num", "rpc", "nevent"], columns="strip")
        df.columns.names = ["feature", "strip"]
        return cls(parsed_config, df)

    def calc_features(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Calculate features on a event analysis class.

        This function will appply aggregated function on the data provided.
        The data should be a pandas dataframe with multicolumn index of the shape
        (feature, strip). The functions applied will run over each row of the
        dataframe and compute global strip features.

        The list of features computed by this function are:
        - event_charge : the sum of collected charge by all the strips
        - cluster_size : the biggest cluster of fired events
        - is_detected : if there is at least one is_detected signal
        - time_peak : the minimum time over which a peak is found
        - event_type: the type of the biggest signal

        Returns
        -------
        pd.DataFrame
            a dataframe with the original index passed by data and a number
            of columns corresponding to the number of computed features
        """
        df = data
        event_charge = df.charge.sum(axis=1)
        cluster_size = (
            df.is_detected.dropna()
            .astype(bool)
            .apply(lambda x: olefin.analysis.utils.clusize(x.values), axis=1)
        )
        event_type = df["type"].max(axis=1)
        detected_signals = df.is_detected.astype(bool)
        is_detected = detected_signals.any(axis=1)
        time_peak = df[detected_signals].time_peak.mean(axis=1)
        event_height = df[detected_signals].height.max(axis=1)
        time_over_threshold = df[detected_signals].time_over_threshold.max(axis=1)

        return pd.DataFrame(
            {
                "event_charge": event_charge,
                "cluster_size": cluster_size,
                "is_detected": is_detected,
                "time_peak": time_peak,
                "event_type": event_type,
                "event_height": event_height,
                "time_over_threshold": time_over_threshold
            }
        )

    def run(self):
        """
        Run the analysis on the class. This method will run compute the
        features and set the result to the .data attribute
        """
        self.data = self.calc_features(self.indata)
        self.data = EventAnalysisDataFrame(self.data)
        return self.data


class EventAnalysisDataFrame(pd.DataFrame):
    """Custom event dataframe mainly for overriding default pandas behaviour
    when plotting dataframes"""

    def olefin_plot(
        self,
        features: t.Optional[t.List] = None,
        query: str = "is_detected",
        features_dict: t.Optional[t.Dict] = None,
        density: bool = True,
        layout: t.Optional[t.Tuple] = None,
        *args,
        **kwargs,
    ):
        """Overridden method. Plots features in subplots. Data is plotted as
        a histogram since it represents distribution.

        Parameters
        ----------
        features: list or None
            a list of features (columns) of the dataframe to be plotted
        query: str
            a string to filter the dataframe. It is passed to the
            pandas.DataFrame.query method directly
        features_dict: dict
            a dict where the keys are the name of the features (columns) and the
            values are dicts to be used by matplotlib. Currently supported
            properties are:
            - bins: str, int, range, np.arange the number of bins
            - title: title of the subplot
            - xscale: the type of x-scale for the subplot
            - yscale: the type of y-scale for the subplot
            - label: the label to put on the x axis
        density: bool = True
            if True normalize the histogram to 1
        layout: tuple
            a tuple indicating the number of rows and the number of columns for
            the subplots. E.g. (2, 2) creates 4 subplots layed out in 2 rows and
            2 columns.

        Returns
        -------
        matplotlib.axes.Axes the plotted axes object

        """
        if not layout:
            nfeatures = len(self.columns)
            ncols = nrows = int(np.sqrt(nfeatures)) + 1
        else:
            ncols, nrows = layout

        features_dict = features_dict or {}
        kwargs.setdefault("dpi", 150)
        kwargs.setdefault("figsize", (4 * ncols, 4 * nrows))

        fig, axs = plt.subplots(nrows, ncols, constrained_layout=True, *args, **kwargs)
        features = features or self.columns
        n_features = len(features)

        for ix, ax in enumerate(axs.flat):
            if ix < n_features:
                feature = features[ix]
                data = self.query(query)[feature]
                bins = features_dict.get("bins", "sqrt")

                ax.hist(data, density=density, histtype="step", bins=bins)
                ax.set_title(feature)
                xscale = features_dict.get("xscale", "linear")
                yscale = features_dict.get("yscale", "linear")
                ax.set_xscale(xscale)
                ax.set_yscale(yscale)
                label = features_dict.get("label", "")
                ax.set_xlabel(label)
            else:
                fig.delaxes(ax)
                ax.set_axis_off()

        return fig, axs
