"""olefin - A framework for analysing RPC waveform data for EPDT Gas System group"""
import olefin.analysis.interface
import olefin.analysis.configuration
import olefin.analysis.utils

try:
    import olefin.legacy
except Exception as e:
    import logging

    logging.warning(f"Could not import olefin.legacy: {e}")
from olefin.analysis.implementations.signal import (
    SignalAnalysis,
    EPDTSignalAnalysis,
    TriggerAnalysis,
    RateSignalAnalysis,
)
from olefin.analysis.implementations.event import (
    EventAnalysis,
    EPDTEventAnalysis,
    EventAnalysisDataFrame,
)
from olefin.analysis.implementations.acquisition import (
    AcquisitionAnalysis,
    EPDTAcquisitionAnalysis,
    AcquisitionAnalysisDataFrame,
    RateAcquisitionAnalysis,
)
from olefin.analysis.implementations.run import (
    RunAnalysis,
    EPDTRunAnalysis,
    RateRunAnalysis,
)

from_wavefile = SignalAnalysis.from_wavefile
read_wavefile = SignalAnalysis.read_wavefile
event_from_folder = EventAnalysis.from_folder
acquisition_from_folder = AcquisitionAnalysis.from_folder
from_path = RunAnalysis.from_path
from_saved = RunAnalysis.from_saved

config = olefin.analysis.configuration.config
SignalTypes = olefin.analysis.utils.SignalTypes

__version__ = "1.0.11"
__author__ = "Gianluca Rigoletti <gianluca.rigoletti@cern.ch>"
__all__ = []
