import setuptools
from setuptools.extension import Extension
import logging
import os


# FIXME it errors when reading the README.md file
# with open("README.md", "r") as fh:
#     long_description = fh.read()


if (
    os.name != "nt" and os.name != "posix"
):  # FIXME the posix part is mine to debug and not compile the python code
    # need to check with Gianluca what is nt, maybe windows and see because it is for the legacy code only so
    # probably we can drop the cython part
    from Cython.Build import cythonize
    import numpy

    extensions = [
        Extension(
            "olefin.legacy.read_header_file",
            ["olefin/legacy/read_header_file.pyx"],
            include_dirs=[numpy.get_include()],
            optional=True,
        ),
    ]
    cythonization = cythonize(extensions)
    install_requires = ["cython"]
else:
    logging.warning(f"Not compile cython module")
    cythonization = None
    install_requires = []

setuptools.setup(
    name="olefin",
    version="1.0.11",
    author="Gianluca Rigoletti",
    description="A framework for analysing RPC waveform data for EP-DT Gas System group.",
    long_description="check README.md",  # FIXME put here the readme contents
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=["Programming Language :: Python :: 3",],
    ext_modules=cythonization,
    setup_requires=["cython"],
    extras_require={"root-numpy": "root-numpy"},
    install_requires=install_requires
    + [
        "numpy",
        "pandas>1",
        "matplotlib",
        "pytest",
        "pytest-cov",
        "python-box",
        "numba",
        "pyyaml",
        "scipy",
        "openpyxl",
        "tqdm",
        "psycopg2-binary"
    ],
)
