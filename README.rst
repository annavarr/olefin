olefin
====

.. image:: https://img.shields.io/pypi/v/olefin.svg
    :target: https://pypi.python.org/pypi/olefin
    :alt: Latest PyPI version

.. image:: False.png
   :target: False
   :alt: Latest Travis CI build status

A framework for analysing RPC waveform data for EPDT Gas System group

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`olefin` was written by `Gianluca Rigoletti <gianluca.rigoletti@cern.ch>`_.
