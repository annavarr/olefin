from olefin.analysis.interface import BaseAnalysis
from abc import ABCMeta


def test_base_analysis():
    BaseAnalysis.__abstractmethods__ = set()
    assert isinstance(BaseAnalysis, ABCMeta)

    class DerivedAnalysis(BaseAnalysis):
        pass

    dummy = DerivedAnalysis()
    dummy.run()
    assert hasattr(dummy, "data")
    assert hasattr(dummy, "run") and callable(getattr(dummy, "run"))
    assert hasattr(dummy, "config")
