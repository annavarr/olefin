import pytest
import numpy as np
import pandas as pd
import olefin
import matplotlib as mpl


def test_acquisition_dataframe(run_path, hv_path):
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    acquisition_analysis = olefin.AcquisitionAnalysis.from_folder(
        config=config, folder=hv_path, event_class=olefin.EventAnalysis
    )
    assert isinstance(acquisition_analysis, olefin.AcquisitionAnalysis)
    assert hasattr(acquisition_analysis, "event")
    assert isinstance(acquisition_analysis.event, olefin.EventAnalysis)
    assert acquisition_analysis.indata is acquisition_analysis.event.data
    assert not acquisition_analysis.event.data.empty
    acquisition_analysis.run()
    assert isinstance(acquisition_analysis.data, pd.DataFrame)
    assert not acquisition_analysis.data.empty


def test_from_class(run_path, hv_path):
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    config.acquisition.trigger = "wave0"
    config.acquisition.record_length = 520
    event_analysis = olefin.event_from_folder(config, hv_path)
    event_analysis.run()
    acquisition_analysis = olefin.AcquisitionAnalysis(config, event_analysis.data)
    acquisition_analysis.event = event_analysis
    assert isinstance(acquisition_analysis, olefin.AcquisitionAnalysis)
    assert hasattr(acquisition_analysis, "event")
    assert isinstance(acquisition_analysis.event, olefin.EventAnalysis)
    assert acquisition_analysis.indata is acquisition_analysis.event.data
    assert not acquisition_analysis.event.data.empty
    acquisition_analysis.run()
    assert isinstance(acquisition_analysis.data, pd.DataFrame)
    assert not acquisition_analysis.data.empty


def test_epdt_acquisition_analysis(run_path, hv_path):
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    config.acquisition.trigger = "wave0"
    config.acquisition.record_length = 520
    acquisition_analysis = olefin.EPDTAcquisitionAnalysis.from_folder(config, hv_path)
    assert hasattr(acquisition_analysis, "indata")
    acquisition_analysis.run()
    assert hasattr(acquisition_analysis, "data")
    assert isinstance(acquisition_analysis.data, pd.DataFrame)
    assert set(acquisition_analysis.data.columns) == set(
        [
            "efficiency",
            "efficiency_error",
            "streamer_probability",
            "streamer_probability_error",
            "avalanche",
            "avalanche_error",
            "streamer",
            "streamer_error",
            "prompt_charge",
            "prompt_charge_error",
            "cluster_size",
            "cluster_size_error",
            "cluster_size_avalanche",
            "cluster_size_avalanche_error",
            "cluster_size_streamer",
            "cluster_size_streamer_error",
            "time_res",
            "time_res_error",
            "time_res_chi2",
            "height",
            "height_error",
            "time_over_threshold",
            "time_over_threshold_error"
        ]
    )

    print(acquisition_analysis.data)


def test_time_resolution(run_analysis):
    empty_times = np.array([])
    time_resolution = 2
    result = olefin.EPDTAcquisitionAnalysis.calc_res_time(empty_times, time_resolution)
    assert result == {
        "time_res": np.nan,
        "time_res_error": np.nan,
        "time_res_chi2": np.nan,
    }
    # Check that changing time resolution of the digitizer is actually changing
    # time resolution result
    data = run_analysis.event_data.query("is_detected").time_peak
    time_resolution = 1
    result_1 = olefin.EPDTAcquisitionAnalysis.calc_res_time(data, time_resolution)
    time_resolution = 2
    result_2 = olefin.EPDTAcquisitionAnalysis.calc_res_time(data, time_resolution)
    assert result_1["time_res"] != result_2["time_res"]


def test_different_time_resolution_configuration(hv_path, run_path):
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    default_config = olefin.config
    config.merge_update(default_config)
    # default config
    aa_default = olefin.EPDTAcquisitionAnalysis.from_folder(
        config=config, folder=hv_path, event_class=olefin.EPDTEventAnalysis
    )
    aa_default.run()
    # Change config
    config.acquisition.time_resolution_bins = 1
    aa_v1 = olefin.EPDTAcquisitionAnalysis.from_folder(
        config=config, folder=hv_path, event_class=olefin.EPDTEventAnalysis
    )
    aa_v1.run()
    assert np.any(aa_default.data.time_res != aa_v1.data.time_res)
    

@pytest.mark.parametrize("overlap", ["rpc", "run_name"])
def test_olefin_plot(run_analysis, overlap):
    fig, axs = run_analysis.acquisition_data.olefin_plot(overlap=overlap)
    assert isinstance(fig, mpl.figure.Figure)
    assert isinstance(axs, np.ndarray)
    for ax in axs.flat:
        assert isinstance(ax, mpl.axes.Axes)
    assert len(ax.lines)
    for line in ax.lines:
        assert len(line.get_xydata())
