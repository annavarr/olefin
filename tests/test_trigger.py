import numpy as np
import pandas as pd
import olefin


def test_trigger(hv_path):
    wavefile = hv_path / "wave0.txt"
    config = olefin.config
    trigger_analysis = olefin.TriggerAnalysis.from_wavefile(config, wavefile)
    assert hasattr(trigger_analysis, "indata")
    assert isinstance(trigger_analysis.indata, np.ndarray)
    trigger_analysis.run()
    assert hasattr(trigger_analysis, "data")
    assert isinstance(trigger_analysis.data, pd.DataFrame)
    assert not trigger_analysis.data.empty
