import numpy as np
import pandas as pd
from pathlib import Path
import olefin
import matplotlib as mpl


def test_class_factory(run_path, hv_path):
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    event_analysis = olefin.event_from_folder(
        config, hv_path, signal_class=olefin.SignalAnalysis
    )
    assert hasattr(event_analysis, "indata")
    assert isinstance(event_analysis.indata, pd.DataFrame)


def test_class_run(run_path, hv_path):
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    event_analysis = olefin.event_from_folder(
        config, hv_path, signal_class=olefin.SignalAnalysis
    )
    assert event_analysis.data is None
    data = event_analysis.run()
    assert isinstance(data, pd.DataFrame)
    assert len(data.columns)
    assert set(data.index.names) == {"folder_num", "rpc", "event"}
    assert event_analysis.indata.columns.names == ["feature", "strip"]
    # Test logging warning without one strip
    config.acquisition.strip_map.pop("wave6")
    event_analysis = olefin.event_from_folder(
        config, hv_path, signal_class=olefin.SignalAnalysis
    )
    event_analysis.run()


def test_epdt_event_analysis(run_path, hv_path):
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    event_analysis = olefin.EPDTEventAnalysis.from_folder(
        config, hv_path, signal_class=olefin.EPDTSignalAnalysis
    )
    assert event_analysis.data is None
    data = event_analysis.run()
    assert isinstance(data, pd.DataFrame)
    assert isinstance(event_analysis.data, pd.DataFrame)
    assert all(
        data.columns
        == ["event_charge", "cluster_size", "is_detected",
            "time_peak", "event_type", "event_height", "time_over_threshold"]
    )
    assert hasattr(event_analysis, "indata")
    assert set(event_analysis.indata.index.names) == {"folder_num",
                                                      "rpc", "nevent"}
    assert set(event_analysis.data.index.names) == {"folder_num",
                                                    "rpc", "nevent"}
    assert event_analysis.indata.columns.names == ["feature", "strip"]


def test_epdt_with_trigger(run_path, hv_path):
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    config.acquisition.trigger = "wave0"
    event_analysis = olefin.EPDTEventAnalysis.from_folder(
        config, hv_path, signal_class=olefin.EPDTSignalAnalysis
    )
    event_analysis.run()
    assert event_analysis.data.time_peak.mean() < 0


def test_epdt_without_trigger(run_path, hv_path):
    config = olefin.EPDTRunAnalysis.config_from_folder(run_path)
    config.acquisition.trigger = None
    event_analysis = olefin.EPDTEventAnalysis.from_folder(
        config, hv_path, signal_class=olefin.EPDTSignalAnalysis
    )
    event_analysis.run()


def test_olefin_plot(run_analysis):
    fig, axs = run_analysis.event_data.olefin_plot()
    assert isinstance(fig, mpl.figure.Figure)
    assert isinstance(axs, np.ndarray)
    for ax in fig.axes:
        assert isinstance(ax, mpl.axes.Axes)
        assert len(ax.patches)
        for patch in ax.patches:
            assert len(patch.get_xy())


if __name__ == "__main__":
    path = Path(
        "/home/gianluca/ghg-studies/BackupRPC/256/20210421/" +
        "no-header-512/Cosmics21/MIX0/HV4/"
    )
    config = olefin.analysis.utils.config_from_path(
        "/home/gianluca/ghg-studies/BackupRPC/256/20210421/" +
        "no-header-512/Cosmics21/olefin_config.yaml"
    )
    event_analysis = olefin.EPDTEventAnalysis.from_folder(
        config, path, signal_class=olefin.EPDTSignalAnalysis
    )
    event_analysis.run()
