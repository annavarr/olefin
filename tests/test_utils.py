import pytest
import olefin.analysis.utils as u
import numpy as np
import pandas as pd


def test_nb_expander():
    x = np.array([1, 2, 3])
    f = u.nb_expander(x)
    assert np.array_equal(f, np.array([[1, 2, 3]]))
    x = np.array([[1, 2, 3], [4, 5, 6]])
    g = u.nb_expander(x)
    assert np.array_equal(g, x)


@pytest.mark.parametrize(
    "event_array,result",
    [
        (np.array([1, 0, 1, 1, 1, 0, 0]), np.array([3])),
        (np.array([0, 0, 0, 0, 0]), np.array([0])),
        (np.array([1, 1, 1, 1, 1, 1, 1]), np.array([7])),
        (np.array([0]), np.array([0])),
        (np.array([1]), np.array([1])),
        (
            np.array([[0, 1, 1, 0, 0], [0, 0, 0, 0, 0], [1, 1, 1, 1, 1]]),
            np.array([2, 0, 5]),
        ),
    ],
)
def test_clusize(event_array, result):
    r = u.clusize_algorithm(event_array)
    assert isinstance(r, np.ndarray)
    assert np.array_equal(r, result)

    r = u.clusize(event_array)
    assert isinstance(r, (int, np.ndarray, np.int16))


def test_parse_currents_autogen(run_path):
    with pytest.raises(ValueError):
        filepath = run_path / "currents.obj"
        u.parse_currents_autogen(filepath)


def test_fit_model():
    def model(x, m, q):
        return m*x + q
    initial_params = [2, 0]
    x = np.array([0, 2, 4, 6, 8])
    y = np.array([0, 1, 2, 3, 4])
    popt, pcov, chi2 = u.fit_model(x, y, model, initial_params)
    assert all(not pd.isna(item) for item in popt)
    assert all(not pd.isna(item) for item in pcov)
    assert not pd.isna(chi2)
    x = np.array([0, 2, 4, 6, np.nan])
    popt, pcov, chi2 = u.fit_model(x, y, model, initial_params)


def test_config_from_path(run_path):
    u.config_from_path(run_path / "olefin_config.yaml")
    with pytest.raises(ValueError):
        u.config_from_path(run_path / "olefin_config.pdf")


def test_get_run_name(run_analysis):
    run_name = u.get_run_name(run_analysis.config)
    assert run_name == 'subset_50_events_complete_structure'


def test_get_gas_label(run_analysis):
    gas_label = u.get_gas_label(run_analysis.config)
    assert gas_label == 'R134A/IC4H10/SF6 95.2/4.5/0.3'
