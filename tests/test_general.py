"""Tests general behaviour of a real and complete run analysis
"""
import olefin
import pandas as pd
from pathlib import Path
import logging


logger = logging.getLogger()
logger.setLevel(logging.INFO)


def test_data_access(run_path, run_analysis):
    assert isinstance(run_analysis, olefin.EPDTRunAnalysis)
    analysis = olefin.from_path(run_path, keep_data=True)
    analysis.run()
    logger.info(analysis.data)
    logger.info(analysis.acquisition_data)
    logger.info(analysis.signal_data)
    logger.info(analysis.event_data)


def test_parse_currents(run_path):
    # Assert at least one currents file exists
    currents_files = list(run_path.glob("currents*"))
    assert len(currents_files)
    for currents in ["currents.csv", "currents.xlsx", "currents_test.xlsx"]:
        currents_path = run_path / currents
        if not currents_path.exists():
            continue
        if currents_path.suffix == ".xlsx":
            index_col = False
        else:
            index_col = 0
        df = olefin.analysis.utils.parse_currents_autogen(
            currents_path, index_col=index_col
        )
        logging.info(df)
        assert isinstance(df, pd.DataFrame)


def test_analysis_without_efficiency_data(data_path):
    run_path = data_path / 'subset_50_events_missing_MIX0'
    run_analysis = olefin.EPDTRunAnalysis.from_path(run_path)
    run_analysis.run()


if __name__ == "__main__":
    path = Path("/home/gianluca/Downloads/20210201/no-header-512/Cosmics0/")
    analysis = olefin.from_path(path)
    analysis.run()
